angular.module('override.overrides', []).value('cgBusyDefaults',{
    message:'Loading...',
    backdrop: true,
    templateUrl: 'views/commons/view_partials/cgBusy/cgbusy.tpl.html',
    minDuration: 700
});