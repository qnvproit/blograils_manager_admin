describe('Service:Authentication tests', function () {
    var authenticationService;
    var $location;
    var $q;
    var $rootScope;
    var mockHttpRequest;
    var mockApiService;
    var mockSessionService;
    var mockLocationService;

    var domain = 'http://posapi.bos-vn.localsite/';
    var kickoutPath = 'kickout';

    var sessionServiceData = {
        token: null,
        logo: null,
        name: null,
        tenant: null,
        permissions: null,
        userData: null,
    };

    var requestSuccessData = {};
    var requestFailureData = {};
    var requestStatus = 200;
    var requestSuccessHandler = function(){};
    var requestFailureHandler = function(){};

    beforeEach(function(){  
        module('services.authentication');

        //we need to fake the session service since this is the custom service.
        mockHttpRequest = function(url, data, successHandler, failureHandler){
            angular.extend(this, {
                url: url,                                                   //request url
                data: data,                                                 //request data
                successHandler: successHandler,                             //success handler
                failureHandle: failureHandler,                              //failure handler

                request: function(){
                    if(requestStatus == 200){
                        successHandler(requestSuccessData, 200, requestSuccessHandler, requestFailureHandler);
                    }
                    else{
                        failureHandler(requestFailureData, 500, requestSuccessHandler, requestFailureHandler);
                    }
                }
            });
            
        };

        mockApiService = {
            //fake the function, return a string as url for login
            apiUrl: function(value){
                return 'loginUrl';
            }
        };

        mockSessionService = {
            saveToken: function(value){
                sessionServiceData.token = value;
            },
            getToken: function(){
                return sessionServiceData.token;
            },
            saveLogo: function(value){
                sessionServiceData.logo = value;
            },
            saveName: function(value){
                sessionServiceData.name = value;
            },
            saveTenant: function(value){
                sessionServiceData.tenant = value;
            },
            savePermissions: function(value){
                sessionServiceData.permissions = value;
            },
            saveUserData: function(value){
                sessionServiceData.userData = value;
            },
            removeToken: function(){

            }
        };

        mockLocationService = {
            kickout: function(){
                $location.path(kickoutPath);
            },
        }

        module(function ($provide) {
            $provide.value('httpRequest', mockHttpRequest);
            $provide.value('apiService', mockApiService);
            $provide.value('sessionService', mockSessionService);
            $provide.value('locationService', mockLocationService);
        });

        inject(function (_authenticationService_, _$location_, _$q_, _$rootScope_) { 
            authenticationService = _authenticationService_;
            $location = _$location_;
            $q = _$q_;
            $rootScope = _$rootScope_;
        })
    });

    it('When signin is called and return without any failure, we need to be able to extract right data and put into session, and the success handler got called', function () {

        requestSuccessData = {
            error: 0,
            message: "Login Successfully.",
            data: {
                access_token: "XOGzgkrFdrAJdzGUz7x8UtIyEZkr0d",
                active: "1",
                address: "2802 Chung cu song da, Nguyen Trai, Thanh Xuan, Ha Noi",
                avatar: "zYh8WpuCkbFEho6SEVem.jpg",
                avatar_url: "http://posapi.bos-vn.localsite/userdata/images/roxwin/avatar//zYh8WpuCkbFEho6SEVem.jpg",
                create_date: "1397581200",
                email: "quocviet.cntt.bk@gmail.com",
                name: "Viet Quoc",
                permissions: '["BROWSER_POS_USER","EDIT_POS_USER","REMOVE_POS_USER","BROWSER_POS_GLO_PER","BROWSER_POS_CUSTOMER","EDIT_POS_CUSTOMER","REMOVE_POS_CUSTOMER","BROWSER_POS_ORDER","EDIT_POS_ORDER","REMOVE_POS_ORDER","BROWSER_POS_PRODUCT","EDIT_POS_PRODUCT","REMOVE_POS_PRODUCT","BROWSER_POS_ALL_SHOP","EDIT_POS_SHOP","REMOVE_POS_SHOP","BROWSER_POS_SHOP","EDIT_EVENT_SHOP","REMOVE_EVENT_SHOP","SYSTEM_FINANCIAL_ACCESS","VIEW_FINANCIAL_DATA","EDIT_FINANCIAL_DATA","BROWSER_EVENT_SHOP","BROWSER_EVENT_SYSTEM"]',
                phone: "+84973668874",
                tenant_logo: "http://bos.localsite/images/system/Client/Logo/Agritrack.png",
                user_id: "1",
            },
        };

        var loginData = {
            tenant: 'tenant',
        }

        requestStatus = 200;

        var isSuccessHandlerCalled = false;
        var successHandler = function(){
            isSuccessHandlerCalled = true;
        }

        authenticationService.signIn(loginData, successHandler, null);

        $rootScope.$digest();

        //success handler have to be called
        expect(isSuccessHandlerCalled).toEqual(true);

        //session have to be saved
        expect(sessionServiceData.token).toEqual(requestSuccessData.data.access_token);
        expect(sessionServiceData.logo).toEqual(requestSuccessData.data.tenant_logo);
        expect(sessionServiceData.name).toEqual(requestSuccessData.data.name);
        expect(sessionServiceData.tenant).toEqual(loginData.tenant);
        expect(sessionServiceData.permissions).toEqual(requestSuccessData.data.permissions);
        expect(sessionServiceData.userData).toEqual(angular.toJson(requestSuccessData.data));
    });

    it('When signin is called and return with failure, remove token have to be called, and failure handler will also be called', function () {
        requestFailureData = {
            error: 0,
            message: "Login failure.",
        };

        var loginData = {
            success: false
        }

        requestStatus = 500;
        
        var isFailureHandlerCalled = false;
        var failureHandler = function(){
            isFailureHandlerCalled = true;
        }

        spyOn(mockSessionService, 'removeToken');    

        authenticationService.signIn(loginData, null, failureHandler);

        $rootScope.$digest();

        //success handler have to be called
        expect(isFailureHandlerCalled).toEqual(true);

        //remove token have to be called
        expect(mockSessionService.removeToken).toHaveBeenCalled();
    });

    it('When signout is called and return with success, remove token have to be called, and user will be redirected to kickout path', function () {
        //spy on path
        spyOn($location, 'path');  
        spyOn(mockSessionService, 'removeToken');    

        requestSuccessData = {
            error: 0,
            message: "Logout Successfully.",
            data: [],
        };

        var logoutData = {
            accessToken: 'token',
        }

        requestStatus = 200;

        var isSuccessHandlerCalled = false;
        var successHandler = function(){
            isSuccessHandlerCalled = true;
        };

        authenticationService.signOut(successHandler, null);

        $rootScope.$digest();

        //success handler have to be called
        expect(isSuccessHandlerCalled).toEqual(true);

        //remove token have to be called
        expect(mockSessionService.removeToken).toHaveBeenCalled();

        //user have to be redirected to kickout page
        expect($location.path).toHaveBeenCalledWith(kickoutPath);
    });

    it('When signout is called and return with failure, remove token still have to be called, and user will be redirected to kickout path', function () {
        //spy on path
        spyOn($location, 'path');  
        spyOn(mockSessionService, 'removeToken');    

        requestFailureData = {
            error: 1,
            message: "Logout Failure.",
            data: [],
        };

        var logoutData = {
            accessToken: 'token',
        }

        requestStatus = 500;

        var isFailureHandlerCalled = false;
        var failureHandler = function(){
            isFailureHandlerCalled = true;
        }

        authenticationService.signOut(null, failureHandler);

        $rootScope.$digest();

        //success handler have to be called
        expect(isFailureHandlerCalled).toEqual(true);

        //remove token have to be called
        expect(mockSessionService.removeToken).toHaveBeenCalled();

        //user have to be redirected to kickout page
        expect($location.path).toHaveBeenCalledWith(kickoutPath);
    });
});