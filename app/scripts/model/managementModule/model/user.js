angular.module('app.model.user', []).factory('userModel', function(sessionService){
	var userModel = function(data) {
		var thisModel = this;											//hold an object which is actually this service

        //set defaults properties and functions
        angular.extend(this, {
        	/* ---------- properties ---------- */
            user_id: (data && data.user_id)?data.user_id:null,
            password: (data && data.password)?data.password:null,
            name: (data && data.name)?data.name:null,
            email: (data && data.email)?data.email:null,
            create_date: (data && data.create_date)?data.create_date:null,
            phone: (data && data.phone)?data.phone:null,
            address: (data && data.address)?data.address:null,
            avatar: (data && data.avatar)?data.avatar:null,
            avatar_url: (data && data.avatar_url)?data.avatar_url:null,
            roles: (data && data.roles)?data.roles:null,
            active: (data && data.active)?data.active:false,						

            /* ---------- functions ---------- */
            isCurrentUser: function(){
                return (this.user_id == sessionService.getUserData().user_id);
            },
        });
    };
    return userModel;
});