angular.module('app.mangement.createpost', [])
.controller('createPostCtrl', [
    '$scope', 
    'ajaxService', 
    'apiUrl',
    'sessionService',
    '$modal',
    'logger',
    function($scope, ajaxService, apiUrl, sessionService, $modal, logger) {

        $scope.isBusy = false;
        $scope.message = ""
        $scope.breadcrumbs = [
            {
                href: '#/blog/post/list',
                label: 'List posts'
            },
            {
                href: '#/blog/post/create',
                label: 'Create posts'
            },
        ];

        $scope.suggestTags = [
        ];

        // $scope.tags = [];

        $scope.post = {
            title: "",
            content: "",
            status: "",
            category_id: "",
            tags: []
        }

       

        $scope.getListTags = function() {
            ajaxService.request(
                apiUrl.listTag,
                {
                    access_token: sessionService.getToken()
                }, function(data) {
                    $scope.suggestTags = data.data;
                }, function(data) {

                }
            );
        }


        $scope.createNewPost = function() {
            
            $scope.post['access_token'] = sessionService.getToken();

            console.log($scope.post);

            ajaxService.request(
                apiUrl.createPost,
                $scope.post
                , function(data) {
                    $scope.message = data.message;
                    logger.log(data.message);
                }, function(data) {

                }
            )
        }


        $scope.getListTags();
        

    }
]);