angular.module( 'directives.qSortCols', [] ).directive( 'qSortCols', [ function() {
	return {
		restrict: 'E',
		replace: true,
		templateUrl: "scripts/commons/directives/qSortCols/template/qSortCols.html",
		scope: {
			config: '=',
			attr: '='
		},
		controller: function( $scope ) {

		}
	}
}])