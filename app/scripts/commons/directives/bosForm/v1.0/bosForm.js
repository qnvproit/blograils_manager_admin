angular.module('directives.bosForm', [])

/* Service to support form field */
.factory('bosFormGeoServices', function($http, $location){
  return  {
    listCountries:  function(successCallback, failCallback){  
      $http.get('scripts/commons/directives/bosForm/v1.0/resources/jsons/geo/countries.json').success (function(data){
          successCallback(data);
      }
      ).error(function(){
          failCallback("fail to load list countries");
      }
      );
    },
    getCountryName: function(searchKey, successCallback, failCallback){
      $http.get('scripts/commons/directives/bosForm/v1.0/resources/jsons/geo/countries.json').success (function(data){
          var continueLoop = true;
          angular.forEach(data, function(value, key) {
              if(value && continueLoop){
                  if(value.cca2 == searchKey){
                      successCallback(value.name)
                      continueLoop = false;
                  }
              }
          });
          if(continueLoop == true){
              failCallback("fail to get country name");
          }
      }
      ).error(function(){
          failCallback("fail to load list countries");
      }
      );
    }
  }
})

/* Custom field validation */
.directive('groupValidations', function ($compile) {
  return {
    restrict: 'A',
    replace: false,
        terminal: true,
        terminal: true,
        priority: 1000,
    link: function (scope,element, attrs) {
      var validations = scope.$eval(attrs.groupValidations);
      var needReload = false;
      if(validations['required'] && validations['required'] == true){
        element.attr("required", true);
        needReload = true;
      }
      else{
        element.attr("required", false);
        needReload = true;
      }

      if(validations['data-ng-minlength'] && validations['data-ng-minlength'] > 0){
        element.attr("data-ng-minlength", validations['data-ng-minlength']);
        needReload = true;
      }

      if(validations['data-ng-maxlength'] && validations['data-ng-maxlength'] >= 0){
        element.attr("data-ng-maxlength", validations['data-ng-maxlength']);
        needReload = true;
      }

      if(validations['data-max']){
        element.attr("data-max", validations['data-max']);
        needReload = true;
      }

      if(validations['data-min']){
        element.attr("data-min", validations['data-min']);
        needReload = true;
      }

      if(validations['data-ng-pattern']){
        element.attr("data-ng-pattern", validations['data-ng-pattern']);
        needReload = true;
      }

      if(validations['data-validate-equals']){
        element.attr("data-validate-equals", "formData['"+validations['data-validate-equals']+"']");
        needReload = true;
      }

      if(needReload == true){
        element.removeAttr("group-validations");
        $compile(element)(scope);
      }
    }
  };
 })

/* The angular form, auto generate via configuration */
.directive('bosForm', [
  function() {
    return {
      restrict: 'E',
      scope: {
          config: '=',
          control: '=',
      },
      templateUrl: 'scripts/commons/directives/bosForm/v1.0/tpl/bosForm.html',
      replace: true,
      controller: function($scope, $location, $modal, $http, bosFormGeoServices){
          /* ==================== CONFIGURE PARAMS ===================== */
          $scope.id = $scope.config.id;
          $scope.name = $scope.config.name;                                                       //form name
          $scope.token = $scope.config.token;                                                     //form token
          $scope.postRequest = $scope.config.postRequest;                                         //form action config
          $scope.headerType = ($scope.config.headerType)?$scope.config.headerType:'json';         //type of header: json or form
          $scope.style = $scope.config.style;

          $scope.customSubmit = ($scope.config.customSubmit)?$scope.config.customSubmit:false;

          $scope.lblSubmit = ($scope.config.lblSubmit)?$scope.config.lblSubmit:'Submit';          //label of the submit button
          $scope.lblReset = ($scope.config.lblReset)?$scope.config.lblReset:'Reset';          //label of the submit button
          $scope.hideFormSubmitControls = ($scope.config.hideFormSubmitControls)?$scope.config.hideFormSubmitControls:false;          //will hide the buttons or controls at the bottoms of the form?

          $scope.fields = $scope.config.fields;
          $scope.fieldsExtraData = {
            country : {
              countries : [],                                                                 //list of countries
            },
          };                                                                                  //hold the data for some special field

          /* ==================== PARAMS ===================== */
          //hold the form data to upload to api, field without model won't be added to this
          $scope.formData = {
            accessToken: $scope.token,
          }
          angular.forEach($scope.fields, function(value, key) {
            if(value){
              if(value.model){
                $scope.formData[value.model] = (value.defaultValue != null)?value.defaultValue:null;
              }
            }
          });

          $scope.postStatus = {
            status: null,                       //null, fail, success, uploading
            message: null,
          };

          /* ==================== CONFIG FIELDS ===================== */
          //config the watcher for field with it
          angular.forEach($scope.fields, function(value, key) {
            if(value){
              if(value.valueWatcher){
                $scope.$watch('formData.'+value.model, function() {
                  value.valueWatcher($scope.formData);
                });
              }
            }
          });
          /* ================== functions ================== */
          //return the form data
          $scope.getFormData = function(){
            return $scope.formData;
          }

          //call the function to prepare data for some of the field type
          $scope.initFields = function(){
            angular.forEach($scope.fields, function(value, key) {
              if(value){
                if(value.type == 'country'){
                  //this is country field, load the city list
                  bosFormGeoServices.listCountries(function(data){
                    $scope.fieldsExtraData.country.countries = data;
                  }, function(errorMessage){
                    console.log("Cannot load country list");
                  });
                }
              }
            });
          };

          //convert the submit data base on the field config
          $scope.convertSubmitData = function(){
            var submitData = {};

            angular.forEach($scope.formData, function(value, key) {
              submitData[key] = value;
            });

            angular.forEach($scope.fields, function(value, key) {
              if(value){
                if(value.type == 'select2'){
                  if(value.optionModel && value.config.multiple == true){
                    var currentValue = $scope.formData[value.model];
                    var newValue = [];

                    angular.forEach(currentValue, function(item, key) {
                      if(item[value.optionModel]){
                        newValue.push(item[value.optionModel]);
                      }
                    });

                    submitData[value.model] = newValue;
                  }
                  else if(value.optionModel && value.config.multiple == false){
                    var currentValue = $scope.formData[value.model];
                    var newValue = (currentValue)?currentValue[value.optionModel]:null;

                    submitData[value.model] = newValue;
                  }
                }
                else if(value.type == 'switch'){
                  if(value.model){
                    var currentValue = $scope.formData[value.model];
                    var newValue = (currentValue == true)?"1":"0";

                    submitData[value.model] = newValue;
                  }
                }
              }
            });

            return submitData;
          } ;

          //get field data
          $scope.getFieldData = function (model){
            return $scope.formData[model];
          }

          //get field data
          $scope.setFieldData = function (model, value){
            return $scope.formData[model] = value;
          }

          //update form field atribute
          $scope.updateFieldData = function(fieldName, attribute, newValue){
            angular.forEach($scope.fields, function(value, key) {
              if(value){
                if(value.name == fieldName){
                  value[attribute] = newValue;
                }
              }
            });
          }

          //reload field config
          $scope.reloadFieldConfig = function(config){
            $scope.fields = config;

            $scope.formData = {
              accessToken: $scope.token,
            }
            angular.forEach($scope.fields, function(value, key) {
              if(value){
                if(value.model){
                  $scope.formData[value.model] = (value.defaultValue != null)?value.defaultValue:null;
                }
              }
            });
          };

          //reset form
          $scope.reset = function(){
            angular.forEach($scope.fields, function(value, key) {
              if(value){
                $scope.formData[value.model] = (value.defaultValue != null)?value.defaultValue:null;
              }
            });

            $scope[$scope.name].$setPristine();
          }

          //check field is invalid or not
          $scope.isFieldValid = function(fieldName){
            var form = $scope[$scope.name];
            if(form[fieldName] == null){
              return true;
            }
            return !(form[fieldName].$invalid && form[fieldName].$dirty);
          };

          //check if we input valid data for login
          $scope.canSubmit = function() {
            return $scope[$scope.name].$valid && $scope.postStatus.status != 'uploading';
          };

          //hide the flash message
          $scope.hideFlashMessage = function(){
            $scope.postStatus.status = null;
          };

          //handler when request success but fail
          $scope.requestSuccessWithErrorHandler = function(data, status, headers, config){
            if(data.error == 1){
              $scope.postStatus.status = 'fail';
              $scope.postStatus.message = data.message;

              if($scope.postRequest.failCallback != null){
                $scope.postRequest.failCallback(data, status, headers, config);
              }
            }
          };

          $scope.requestFailHandler = function(data, status){
            $scope.postStatus.status = 'fail';
            $scope.postStatus.message = data.message;

            if($scope.postRequest.failCallback != null){
              $scope.postRequest.failCallback(data, status, null, null);
            }
          };

          $scope.requestSuccessHandler = function(data, status, headers, config){
            if(data.error != 1){
              $scope.postStatus.status = 'success';

              if($scope.postRequest.successCallback != null){
                $scope.postRequest.successCallback(data, status, headers, config);
              }
            }
          };

          //submit function
          $scope.submit = function(){
            if($scope.customSubmit == false){
              $scope.postStatus.status = 'uploading';

              var submitData = $scope.convertSubmitData();
              console.log($scope.postRequest.extraData);
              if($scope.postRequest.extraData){
                console.log($scope.postRequest.extraData);
                angular.extend(submitData, $scope.postRequest.extraData);
              }

              //add extra data which can be change outside

              if($scope.headerType == 'json'){
                $http.post($scope.postRequest.action, JSON.stringify(submitData), {
                      transformRequest: angular.identity,
                      headers: {'Content-Type': undefined},
                })
                .success(function(data, status, headers, config) {
                  if(data.error == 1){
                    $scope.requestSuccessWithErrorHandler(data, status, headers, config);
                  }
                  else{
                    $scope.requestSuccessHandler(data, status, headers, config);
                  }
                })
                .error(function(data, status, headers, config) {
                  $scope.requestFailHandler(data, status);
                });
              }
              else{
                var formData = new FormData();
                angular.forEach(submitData, function(value, key) {
                  formData.append(key, value);
                });

                $http.post($scope.postRequest.action, formData, {
                      transformRequest: angular.identity,
                      headers: {'Content-Type': undefined},
                })
                .success(function(data, status, headers, config) {
                  if(data.error == 1){
                    $scope.requestSuccessWithErrorHandler(data, status, headers, config);
                  }
                  else{
                    $scope.requestSuccessHandler(data, status, headers, config);
                  }
                })
                .error(function(data, status, headers, config) {
                  $scope.requestFailHandler(data, status);
                });
              }
            }
            else{
              if($scope.config.customSubmitFunction){
                $scope.config.customSubmitFunction($scope.formData);
              }
              else{
                console.log("Custom Submit did not set yet");
              }
            }
          };

          /* ==================== DEFINE CONTROLS ===================== */
          if ($scope.control) {
            $scope.control.reset = $scope.reset;
            $scope.control.updateFieldData = $scope.updateFieldData;
            $scope.control.reloadFieldConfig = $scope.reloadFieldConfig;
            $scope.control.getFieldData = $scope.getFieldData;
            $scope.control.getFormData = $scope.getFormData;
            $scope.control.setFieldData = $scope.setFieldData;
            $scope.control.canSubmit = $scope.canSubmit;
            $scope.control.submit = $scope.submit;
          };

          /* ==================== PRECALL FUNCTIONS ===================== */
          $scope.initFields();

      },
      compile: function(tElement, tAttrs) {
          return function($scope, $elem, $attr) {
              
          }
      }
    };
  }
]);
