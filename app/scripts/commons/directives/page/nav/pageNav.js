angular.module('directives.page.nav', [])

.directive('toggleMinNav', [
  '$rootScope', function($rootScope) {
    //the directive allow the page to toggle show/hide the left menu

    return {
      restrict: 'A',
      link: function(scope, ele, attrs) {
        var $window, Timer, app, updateClass;
        app = $('#app');
        $window = $(window);
        ele.on('click', function(e) {
          if (app.hasClass('nav-min')) {
            app.removeClass('nav-min');
          } else {
            app.addClass('nav-min');
            $rootScope.$broadcast('minNav:enabled');
          }
          return e.preventDefault();
        });
        Timer = void 0;
        updateClass = function() {
          var width;
          width = $window.width();
          if (width < 768) {
            return app.removeClass('nav-min');
          }
        };
        return $window.resize(function() {
          var t;
          clearTimeout(t);
          return t = setTimeout(updateClass, 300);
        });
      }
    };
  }
]).directive('toggleOffCanvas', [
  function() {
    //directive to toggle on off the canvas

    return {
      restrict: 'A',
      link: function(scope, ele, attrs) {
        return ele.on('click', function() {
          return $('#app').toggleClass('on-canvas');
        });
      }
    };
  }
]).directive('collapseNav', [
  function() {
    return {
      restrict: 'A',
      link: function(scope, ele, attrs) {
        var $a, $aRest, $lists, $listsRest, app;
        $lists = ele.find('ul').parent('li');
        $lists.append('<i class="fa fa-caret-right icon-has-ul"></i>');
        $a = $lists.children('a');
        $listsRest = ele.children('li').not($lists);
        $aRest = $listsRest.children('a');
        app = $('#app');
        $a.on('click', function(event) {
          var $parent, $this;
          if (app.hasClass('nav-min')) {
            return false;
          }
          $this = $(this);
          $parent = $this.parent('li');
          $lists.not($parent).removeClass('open').find('ul').slideUp();
          $parent.toggleClass('open').find('ul').slideToggle();
          return event.preventDefault();
        });
        $aRest.on('click', function(event) {
          return $lists.removeClass('open').find('ul').slideUp();
        });
        return scope.$on('minNav:enabled', function(event) {
          return $lists.removeClass('open').find('ul').slideUp();
        });
      }
    };
  }
]).directive('slimScroll', [
  function() {
    return {
      restrict: 'A',
      link: function(scope, ele, attrs) {
        return ele.slimScroll({
          height: '100%'
        });
      }
    };
  }
]).directive('highlightActive', [
  function() {
    return {
      restrict: "A",
      controller: [
        '$scope', '$element', '$attrs', '$location', function($scope, $element, $attrs, $location) {
          var highlightActive, links, path;
          links = $element.find('a.menu_level_1');
          path = function() {
            return $location.path();
          };
          highlightActive = function(links, path) {
            path = '#' + path;
            return angular.forEach(links, function(link) {
              var $li, $link, href;
              $link = angular.element(link);
              $li = $link.parent('li');
              href = $link.attr('href');
              if ($li.hasClass('active')) {
                $li.removeClass('active');
              }

              //get the childrent menu item, check for the matching with given path
              $ulNext = $link.next();
              childlinks = $ulNext.find('a');
              var needHighlight = false;
              var needHighlightParent = false;
              angular.forEach(childlinks, function(childlink){
                var $childlink = angular.element(childlink);
                childHref = $childlink.attr('href');

                if(childHref.toLowerCase() === path.toLowerCase()){
                  //find children which point to current path, meaning the parent need to be highlight
                  needHighlight = true;

                  //set this child li parent as active
                  needHighlightParent = true;

                  //highlight this child
                  $childlink.parent('li').addClass('active')
                }
                else{
                  //highlight this child
                  $childlink.parent('li').removeClass('active')
                }
              });

              if (needHighlight) {
                return ($li.hasClass('open'))?$li.addClass('active'):$li.addClass('active').toggleClass('open').find('ul').slideToggle();
              }

              //now we need to check if parent will be highlight or not
              if(needHighlightParent){
                //one element is perfect match, meaning it will be active
                return ($li.hasClass('open'))?$li.addClass('active'):$li.addClass('active').toggleClass('open').find('ul').slideToggle();
              }
              else{
                //no element is perfect match. We need to check if parent href is a part of the current path of not
                if (path.toLowerCase().indexOf(href.toLowerCase()) === 0) {

                  return ($li.hasClass('open'))?$li.addClass('active'):$li.addClass('active').toggleClass('open').find('ul').slideToggle();
                }
              }
            });
          };
          highlightActive(links, $location.path());
          return $scope.$watch(path, function(newVal, oldVal) {
            if (newVal === oldVal) {
              return;
            }
            return highlightActive(links, $location.path());
          });
        }
      ]
    };
  }
]);