describe('Model Service:UserModel Service Test', function () {
    var userService;
    var $location;
    var $q;
    var $rootScope;
    var mockHttpRequest;
    var mockApiService;
    var mockSessionService;
    var mockLocationService;
    var mockUserModel;

    var domain = 'http://posapi.bos-vn.localsite/';
    var kickoutPath = 'kickout';

    var sessionServiceData = {
        token: 'token',
    };

    var requestSuccessData = {};
    var requestFailureData = {};
    var requestStatus = 200;
    var requestSuccessHandler = function(){};
    var requestFailureHandler = function(){};

    beforeEach(function(){  
        module('app.modelServices.userService');

        //we need to fake the session service since this is the custom service.
        mockHttpRequest = function(url, data, successHandler, failureHandler){
            angular.extend(this, {
                url: url,                                                   //request url
                data: data,                                                 //request data
                successHandler: successHandler,                             //success handler
                failureHandle: failureHandler,                              //failure handler

                request: function(){
                    if(requestStatus == 200){
                        successHandler(requestSuccessData, 200, requestSuccessHandler, requestFailureHandler);
                    }
                    else{
                        failureHandler(requestFailureData, 500, requestSuccessHandler, requestFailureHandler);
                    }
                }
            });
            
        };

        mockApiService = {
            //fake the function, return a string as url for login
            apiUrl: function(value){
                return 'fetchUserUrl';
            }
        };

        mockSessionService = {
            getToken: function(){
                return sessionServiceData.token;
            },
        };

        mockLocationService = {
            errorRedirect: function(value){
                $location.path(kickoutPath);
            },
        };

        mockUserModel = function(data){
            angular.extend(this, {
                user_id: (data && data.user_id)?data.user_id:null,
                password: (data && data.password)?data.password:null,
                name: (data && data.name)?data.name:null,
                email: (data && data.email)?data.email:null,
                create_date: (data && data.create_date)?data.create_date:null,
                phone: (data && data.phone)?data.phone:null,
                address: (data && data.address)?data.address:null,
                avatar: (data && data.avatar)?data.avatar:null,
                avatar_url: (data && data.avatar_url)?data.avatar_url:null,
                roles: (data && data.roles)?data.roles:null,
                active: (data && data.active)?data.active:false,                        
            });
            
        };

        module(function ($provide) {
            $provide.value('httpRequest', mockHttpRequest);
            $provide.value('apiService', mockApiService);
            $provide.value('sessionService', mockSessionService);
            $provide.value('locationService', mockLocationService);
            $provide.value('userModel', mockUserModel);
        });

        inject(function (_userService_, _$location_, _$q_, _$rootScope_) { 
            userService = _userService_;
            $location = _$location_;
            $q = _$q_;
            $rootScope = _$rootScope_;
        })
    });

    it('When fetch user is called and return without any failure, we need to be able to extract right data and put into userModel object, and the success handler got called', function () {

        requestSuccessData = {
            error: 0,
            message: "Successfully get user detail",
            data: {
                active: "1",
                address: "2802 Chung cu song da, Nguyen Trai, Thanh Xuan, Ha Noi",
                avatar: "zYh8WpuCkbFEho6SEVem.jpg",
                avatar_url: "http://posapi.bos-vn.localsite/userdata/images/roxwin/avatar//zYh8WpuCkbFEho6SEVem.jpg",
                create_date: "1397581200",
                email: "quocviet.cntt.bk@gmail.com",
                name: "Viet Quoc",
                password: "fcea920f7412b5da7be0cf42b8c93759",
                phone: "+84973668874",
                roles: ["Administrator"],
                user_id: "1",
            },
        };

        var fetchData = {
            accessToken: 'accessToken',
            userId: 10,
        }

        requestStatus = 200;

        var isSuccessHandlerCalled = false;
        var userModel = null;
        var successHandler = function(user){
            isSuccessHandlerCalled = true;
            userModel = user;
        }

        userService.fetchUser(fetchData.userId, successHandler, null);

        $rootScope.$digest();

        //success handler have to be called
        expect(isSuccessHandlerCalled).toEqual(true);

        //user model have to have similar data with return data
        expect(userModel.email).toEqual(requestSuccessData.data.email);
        expect(userModel.active).toEqual(requestSuccessData.data.active);
        expect(userModel.address).toEqual(requestSuccessData.data.address);
        expect(userModel.avatar).toEqual(requestSuccessData.data.avatar);
        expect(userModel.avatar_url).toEqual(requestSuccessData.data.avatar_url);
        expect(userModel.create_date).toEqual(requestSuccessData.data.create_date);
        expect(userModel.name).toEqual(requestSuccessData.data.name);
        expect(userModel.password).toEqual(requestSuccessData.data.password);
        expect(userModel.phone).toEqual(requestSuccessData.data.phone);
        expect(userModel.roles).toEqual(requestSuccessData.data.roles);
        expect(userModel.user_id).toEqual(requestSuccessData.data.user_id);
    });

    it('When fetch user is called and return with failure, user will be kickout, and failure handler will also be called', function () {
        requestFailureData = {
            error: 1,
            message: "Cannot find this user in your system!",
        };

        var fetchData = {
            accessToken: 'accessToken',
            userId: 10,
        }

        requestStatus = 500;
        
        var isFailureHandlerCalled = false;
        var failureHandler = function(){
            isFailureHandlerCalled = true;
        }

        spyOn($location, 'path'); 

        userService.fetchUser(fetchData.userId, null, failureHandler);

        $rootScope.$digest();

        //failure handler have to be called
        expect(isFailureHandlerCalled).toEqual(true);

        expect($location.path).toHaveBeenCalledWith(kickoutPath);
    });
});