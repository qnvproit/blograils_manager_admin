angular.module( 'directives.qTable', [] ).directive( 'qTable', [ function() {

	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'scripts/commons/directives/qTable/template/qTable.html',
		scope: {
			config : "=",
			data : "="
		},
		link: function( scope, element, attrs ) {

		},
		controller: function( $scope, $element, $modal, $location ) {

			$scope.advancedSearch = null;

			/****************************** List functions ****************************/

			// Advanced Search
			$scope.showAdvancedSearch = function() {
				$scope.advancedSearch = $modal.open({
					templateUrl: '/modal/advancedSearch.html',
					scope: $scope
				})
			}

			// Method get data
			$scope.getDataTable = function() {
				// alert('getting data table . . .');
				$scope.config.getDataTable();

				$element.find('button').css( 'background', '#000' );
			}

			// GotoPage
			$scope.goToPage = function( orderPage ) {
				if( orderPage < 0 ) {
					return;
				}
				$scope.config.page.currentPage = orderPage;
			}

			// GoToFirstPage
			$scope.goToFirstPage = function() {
				$scope.config.page.currentPage = 1;
			}

			// GotoPrevPage
			$scope.gotoPrevPage = function() {
				if( $scope.config.page.currentPage <= 1 ) {
					$scope.config.page.currentPage--;
				}
			}

			// GoToNextPage
			$scope.goToNextPage = function() {
				if( $scope.config.page.maxPage != -1 && $scope.config.page.currentPage >= $scope.config.page.maxPage ) {
					return;
				}
				$scope.config.page.currentPage++;
			}

			// GoToLastPage
			$scope.goToLastPage = function() {
				$scope.config.page.currentPage = $scope.config.page.maxPage;
			}



			/************************ $watch function ***********************/

			$scope.$watch( 'config.page.currentPage', $scope.getDataTable, true  );

			function changeCurrentEntries( newValue, oldValue, scope ) {
				if( scope.config.page.currentPage == 1 ) {
					scope.getDataTable();	
				} else {
					scope.goToFirstPage();
				}
				
			}
			$scope.$watch( 'config.page.currentEntries', changeCurrentEntries, true );

			$scope.$watch( 'config.sortDefault', changeCurrentEntries, true );
		}
	}
}]);