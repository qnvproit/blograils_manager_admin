angular.module('run.pageConfig', []).run(['$rootScope', 'pageSettingService', function ($rootScope, pageSettingService) {
    $rootScope.$on('$routeChangeStart', function (event, currRoute, prevRoute) {
        //set the body class base on the given setting
        if(currRoute.bodyClass){
            pageSettingService.setBodyClass(currRoute.bodyClass);
        }
        else{
            pageSettingService.setBodyClassDefault();
        }

        if(currRoute.showNav){
            pageSettingService.setShowNav(true);
        }
        else{
            pageSettingService.setShowNav(false);
        }

        if(currRoute.bodyStyle){
            pageSettingService.setTemplate(currRoute.bodyStyle);
        }
        else{
            pageSettingService.setTemplateDefault();
        }
    });
}]);