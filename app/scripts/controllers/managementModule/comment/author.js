angular.module('app.mangement.author', [])
.controller('authorCtrl', [
    '$scope', 
    'ajaxService', 
    'apiUrl',
    'sessionService',
    '$modal',
    '$bootbox',
    'logger',
    function($scope, ajaxService, apiUrl, sessionService, $modal, $bootbox, logger) {

        $scope.isBusy = false;

        $scope.breadcrumbs = [
            {
                href: '#/posadmin/hr/users',
                label: 'List Author'
            },
        ];
       

        $scope.search = {
            byPostId: '',
            byContent: '',
            byStatus: '',
            byCreated: ''
        }

        $scope.searchByPost = {
            text: "",
            posts: [],
        }


        // Config qPagination directive
        $scope.pageConfig = {
            getDataTable: function() {
                $scope.getListAuthors();
                // if ($scope.hasFirstLoad) {
                //     $scope.getListUser();
                // }
            },
            entriesPerPage: [ 2, 5, 10, 15, 20, 25 ],
            currentEntries : 2,
            currentPage: 1,
            maxPage: -1,
            items: 0,
        }


        // Config qSortCols directive
        $scope.sortConfig = {
            sortBy: 'email',
            asc: true,
            changeSort: function( attr, asc ) {
                $scope.sortConfig.sortBy = attr;
                $scope.sortConfig.asc = asc;
                $scope.getListAuthors();
            }
        }


        $scope.getListAuthors = function() {
            $scope.isBusy = true;

            ajaxService.request(
                apiUrl.listAuthor,
                {
                    access_token: sessionService.getToken(),
                    page: $scope.pageConfig.currentPage,
                    limit: $scope.pageConfig.currentEntries,
                    sortBy:$scope.sortConfig.sortBy,
                    asc: $scope.sortConfig.asc,
                    search: $scope.search

                }, function(data) {
                    $scope.isBusy = false;
                    if(data.success == true) {
                        $scope.response = data.data;
                        console.log($scope.response);
                        //$scope.listPosts = data.data.data;
                    } else {

                    }
                }, function(data) {

                }
            )
        }

        $scope.deletePost = function(id) {

            $bootbox.confirm("<h4>Are you sure you want to delete this Post ?<h4>", function(result) {
                if( result ) {
                    ajaxService.request(
                        apiUrl.delPost,
                        {
                            access_token: sessionService.getToken(),
                            id: id
                        }, function(data) {
                            if(data.success) {
                                logger.log(data.message);
                            } else {
                                logger.logError(data.message);
                            }
                            $scope.getListPost();
                        }, function(data) { 

                        }
                    );
                    

                }
            });
        }


        $scope.showModalAdvandedSearch = function() {
            $scope.advanceSearchModal = $modal.open({
                templateUrl: "views/main/pages/managementModule/comment/partials/modal/search.html",
                scope: $scope
            })
        }


        $scope.triggerSearch = function() {
            $scope.pageConfig.currentPage = 1;
            $scope.getListComments();
            $scope.advanceSearchModal.dismiss('cancel');
        }

        $scope.resetSearch = function() {
            $scope.search.byTitle = "";
            $scope.search.byContent = "";
            $scope.search.byCreated = "";
            $scope.search.byAuthor = "";
            $scope.search.byPostId = "";

            $scope.searchByPost.text = "";
            $scope.postTitle = "";

            $scope.triggerSearch();
        }


        $scope.getDetailCmt = function(id) {
            ajaxService.request(
                apiUrl.detailCmt,
                {
                    access_token: sessionService.getToken(),
                    id: id
                }, function(data) {
                    $scope.detailCmt = data.data;
                    $scope.detailModal = $modal.open({
                        templateUrl: "views/main/pages/managementModule/comment/partials/modal/detail.html",
                        scope: $scope
                    })
                }, function(data) {

                })
        }

        $scope.saveDetailCmt = function() {
            ajaxService.request(
                apiUrl.setStatusCmt,
                {
                    access_token: sessionService.getToken(),
                    id: $scope.detailCmt.id,
                    status: $scope.detailCmt.status
                }, function(data) {
                    if(data.success) {
                        $scope.cancelDetailModal();
                        $scope.getListComments();
                        logger.log(data.message);
                    } else {
                        logger.logError(data.message);
                        $scope.getListComments();
                    }
                }, function(data) {

                })
        }

        $scope.cancelDetailModal = function() {
            $scope.detailModal.dismiss('cancel');
        }

        $scope.deleteCmt = function(id) {
            $bootbox.confirm("<h4>Are you sure you want to delete this Comment ?<h4>", function(result) {
                    if(result) {
                        ajaxService.request(
                            apiUrl.deleteCmt,
                            {
                                access_token: sessionService.getToken(),
                                id: id
                            }, function(data) {
                                if(data.success) {
                                    $scope.getListComments();
                                    logger.log(data.message);
                                } else {
                                    $scope.getListComments();
                                    logger.logError(data.message);
                                }
                            }, function(data) {

                            })
                    }
            })
        }


        $scope.setSearchByPostId = function(id, title) {
            $scope.search.byPostId = id;
            $scope.postTitle = title;
            $scope.searchByPost.posts = []
        }


        $scope.resetPostTitle = function() {
            $scope.search.byPostId = "";
            $scope.postTitle = "";

        }

        $scope.$watch(function() {
            return $scope.search.byStatus;
        }, $scope.getListComments, true);

        $scope.$watch("searchByPost.text", function(newValue, oldValue) {
            if(newValue != "" && newValue != null) {
                $scope.search.byPostId = "";
                ajaxService.request(
                    apiUrl.suggestPost,
                    {
                        access_token: sessionService.getToken(),
                        text: newValue
                    }, function(data) {
                        if(data.success) {
                            $scope.searchByPost.posts = data.data;
                        } else {

                        }
                    }, function(data) {

                    }
                )
            }
        })


        $scope.getListAuthors();

    }
]);