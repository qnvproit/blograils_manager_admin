angular.module('app.mangement.userDetails')

/** 
* service hold the configuration of the overview tab
*/

.factory('userDetailsOverviewConfiguration', function () {
    var configurations = {};

    /* ------------------ Google map configuration ------------------ */
    /**
    * Get breadcrumbs configure
    */
    configurations.googleMapConfig = function() {
        return {
        	center: {
	            latitude: 45,
	            longitude: -73
	        },
	        zoom: 8,
	        allowClickMarker: true,
	        draggable: "true",
        };
    };

    configurations.googleMapControl = function(){
        return {};
    }

    return configurations;
})

/**
* Overview tab controller. Nest inside detail
*/
.controller('userDetailsOverviewCtrl', [
    '$scope', 
    '$q',
    '$modal',
    '$timeout',
    'userDetailsOverviewConfiguration',
    function($scope, $q, $modal, $timeout, userDetailsOverviewConfiguration) {
    	/* ================== PARAMS ================== */
    	$scope.modalAddressMap = null;										//hold the modal object which contain the map

    	$scope.googleMapConfig = userDetailsOverviewConfiguration.googleMapConfig();
    	$scope.googleMapControl = userDetailsOverviewConfiguration.googleMapControl();

        /* ================== DEFER AND PROMISE ================== */

        /* ================== PAGE EVENT ================== */
        /* the init function, called when page load */
        $scope.pageDidLoad = function(){
            
        }

        /* ================== EVENT HANDLER ================== */
        /* user click the show map button */
        $scope.showUserAddressOnMap = function(){
        	//show modal to add new user
	        $scope.modalAddressMap = $modal.open({
	          	templateUrl: "/views/main/pages/managementModule/hr/user/userDetails/modal/userAddressMapModal.html",
	          	scope: $scope,
	          	windowClass: 'app-form-modal-window'
	        });

	        $scope.modalAddressMap.opened.then(function() {
	          	$timeout(function myFunction() {
	            	$scope.googleMapControl.resize();
	            	$scope.googleMapControl.removeAllMarkers();
	            	$scope.googleMapControl.searchPlaceByName($scope.currentUser.address, true);
	          	},1000);
	            
	        });
        };

        //event when click close button
	    $scope.closeModalAddressMap = function(){
	        if($scope.modalAddressMap){
	          	$scope.modalAddressMap.dismiss();
	        }
	    }

    	/* ================== FUNCTIONS ================== */

        /* ================== PAGE BEHAVIOUS ================== */
        /* the init function, called when page load */
        $scope.pageDidLoad();

    }
]);