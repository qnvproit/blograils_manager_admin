(function() {
  angular.module('directives.bosMap', []).controller('directiveMapController',function($scope){
    $scope.controlClick = function(){
      
    }
  }).directive('bosmap', [
    function() {
      return {
        restrict: 'E',
        scope: {
            config: '=',
            control: '=',
        },
        templateUrl: 'scripts/commons/directives/bosMap/v1.0/tpl/bosMap.html',
        replace: true,
        controller: function($scope, $location, $modal, $element, $timeout, $templateCache, $controller, $compile, $bootbox){
          /* ==================== CONFIGURE PARAMS ===================== */
          $scope.type = ($scope.config.type)?$scope.config.type:'simple';                   //type of map : simple: load map with data input as local params, no google api server request
          $scope.center = $scope.config.center;                                             //center of the map when display
          $scope.zoom = $scope.config.zoom;                                                 //zoom level
          $scope.allowClickMarker = ($scope.config.allowClickMarker)?$scope.config.allowClickMarker:false,
          $scope.draggable = $scope.config.draggable;
          $scope.mapControl = {};
          $scope.overlayControls = {
            controller: $scope,
            visible: ($scope.config.mapControl && $scope.config.mapControl.visible)?$scope.config.mapControl.visible: false,
            controls: {
              enableSearch : ($scope.config.mapControl && $scope.config.mapControl.enableSearch)?$scope.config.mapControl.enableSearch: false,
              enableFocus : ($scope.config.mapControl && $scope.config.mapControl.enableFocus)?$scope.config.mapControl.enableFocus: false,
              focusText : ($scope.config.mapControl && $scope.config.mapControl.focusText)?$scope.config.mapControl.focusText: '',
              focusTag : ($scope.config.mapControl && $scope.config.mapControl.focusTag)?$scope.config.mapControl.focusTag: '',
              enableDirection : ($scope.config.mapControl && $scope.config.mapControl.enableDirection)?$scope.config.mapControl.enableDirection: false,
            }
          };

          $scope.eventsHandlers = {
            mapClicked : ($scope.config.eventsHandlers && $scope.config.eventsHandlers.mapClicked)?$scope.config.eventsHandlers.mapClicked: function(){},
          }

          /* ==================== PARAMS ===================== */
          $scope.markers = [];                                                              //contain all the marker
          $scope.displayLoadingBar = false;
          $scope.willRefresh = false;

          $scope.willDisplayMessage = false;                                                 //will display the message box
          $scope.message = null;                                                            //message box
          $scope.searchBarValue = '';                                                       //hold the search string on search bar

          $scope.polylines = [];

          $scope.mapState = {
            inDirectionMode : false,                                                        //check to see if we are in direction mode or not
            directionCurrentFocusField: null,                                            //start or destination
            showDirectionList : true,
            directionStartPoint: '',
            directionStartPointMarker: null,
            markerAsStartPoint: false,
            directionDestinationPoint: '',
            directionDestinationPointMarker: null,
            markerAsDestinationPoint: false,
          }

          $scope.events = {
                click: function (mapModel, eventName, originalEventArgs) {
                    // 'this' is the directive's scope
                    google.maps.event.trigger(mapModel, "resize");
                    $scope.$apply();

                    $scope.mapClickedHandler(mapModel, eventName, originalEventArgs);
                },
              };

          /* ==================== GOOGLE MAP EVENT FUNCTIONS ===================== */
          $scope.mapClickedHandler = function(mapModel, eventName, originalEventArgs){
            if(eventName == 'click'){
              var e = originalEventArgs[0];
              var clickPosition = {
                lat: e.latLng.lat(),
                lon: e.latLng.lng(),
              };
              $scope.eventsHandlers.mapClicked(mapModel, eventName, originalEventArgs, clickPosition);
            }
          }

          /* ==================== GOOGLE MAP CONTROLS FUNCTIONS ===================== */
          //close direction mode
          $scope.stopDirectionMode = function(){
            $scope.mapState.inDirectionMode = false;
            $scope.mapState.directionCurrentFocusField = null;
            $scope.polylines = [];

            $scope.mapState.directionStartPoint = '';
            $scope.mapState.directionStartPointMarker = null;
            $scope.mapState.markerAsStartPoint = false;
            $scope.mapState.directionDestinationPoint = '';
            $scope.mapState.directionDestinationPointMarker = null;
            $scope.mapState.markerAsDestinationPoint = false;

            //remove any marker we used to tag direction before
            $scope.removeAllMarkersWithTag('direction_custom_search_marker_');
          }

          //show direction
          $scope.showDirection = function(){
            $scope.mapState.directionCurrentFocusField = null;

            if($scope.mapState.directionStartPoint == ''){
              $bootbox.alert("Please select the starting point");
              return;
            }

            if($scope.mapState.directionDestinationPoint == ''){
              $bootbox.alert("Please select the destination point");
              return;
            }

            //remove any marker we used to tag direction before
            $scope.removeAllMarkersWithTag('direction_custom_search_marker_');

            //if both search is selected using marker selector
            if($scope.mapState.markerAsDestinationPoint && $scope.mapState.markerAsStartPoint && $scope.mapState.directionStartPointMarker && $scope.mapState.directionDestinationPointMarker){
              $scope.getDirection($scope.mapState.directionStartPointMarker.latitude, $scope.mapState.directionStartPointMarker.longitude, $scope.mapState.directionDestinationPointMarker.latitude, $scope.mapState.directionDestinationPointMarker.longitude, 'direction', 'direction_', null, null);
            }

            if($scope.mapState.markerAsStartPoint == false || $scope.mapState.markerAsDestinationPoint == false){
              //start point is input as text address
              if($scope.mapState.markerAsStartPoint == false && $scope.mapState.markerAsDestinationPoint && $scope.mapState.directionDestinationPointMarker ){
                $scope.searchPlaceLocation($scope.mapState.directionStartPoint, function(locations){
                  if(locations.length > 0){
                    $scope.addMarker('direction_start_point_' + (i + 1), locations[0].lat, locations[0].lng, locations[0].address, null, 'direction_custom_search_marker_');
                    $scope.getDirection(locations[0].lat, locations[0].lng, $scope.mapState.directionDestinationPointMarker.latitude, $scope.mapState.directionDestinationPointMarker.longitude, 'direction', 'direction_', null, null);
                  }
                  else{
                    $bootbox.alert("Cannot found any location match with the given start point");
                  }
                });
              }

              //destination point is input as text address
              if($scope.mapState.markerAsDestinationPoint == false && $scope.mapState.markerAsStartPoint && $scope.mapState.directionStartPointMarker ){
                $scope.searchPlaceLocation($scope.mapState.directionDestinationPoint, function(locations){
                  if(locations.length > 0){
                    $scope.addMarker('direction_destination_point_' + (i + 1), locations[0].lat, locations[0].lng, locations[0].address, null, 'direction_custom_search_marker_');
                    $scope.getDirection($scope.mapState.directionStartPointMarker.latitude, $scope.mapState.directionStartPointMarker.longitude, locations[0].lat, locations[0].lng, 'direction', 'direction_', null, null);
                  }
                  else{
                    $bootbox.alert("Cannot found any location match with the given destination point");
                  }
                });
              }

              //both is input as text
              if($scope.mapState.markerAsDestinationPoint == false && $scope.mapState.markerAsStartPoint == false){
                $scope.searchPlaceLocation($scope.mapState.directionStartPoint, function(startLocations){
                  if(startLocations.length > 0){
                    $scope.searchPlaceLocation($scope.mapState.directionDestinationPoint, function(destinationLocations){
                      if(destinationLocations.length > 0){
                        $scope.addMarker('direction_start_point_' + (i + 1), startLocations[0].lat, startLocations[0].lng, startLocations[0].address, null, 'direction_custom_search_marker_');
                        $scope.addMarker('direction_destination_point_' + (i + 1), destinationLocations[0].lat, destinationLocations[0].lng, destinationLocations[0].address, null, 'direction_custom_search_marker_');
                        $scope.getDirection(startLocations[0].lat, startLocations[0].lng, destinationLocations[0].lat, destinationLocations[0].lng, 'direction', 'direction_', null, null);
                      }
                      else{
                        $bootbox.alert("Cannot found any location match with the given destination point");
                      }
                    });
                  }
                  else{
                    $bootbox.alert("Cannot found any location match with the given start point");
                  }
                });
              }
            }
          }

          //handle the event when user select a marker for direction
          $scope.selectMarkerForDirection = function(marker){
            if(marker && $scope.mapState.directionCurrentFocusField){
              if($scope.mapState.directionCurrentFocusField == 'start'){
                $scope.mapState.directionStartPoint = marker.text;
                $scope.mapState.directionStartPointMarker = marker;
                $scope.mapState.markerAsStartPoint = true;
              }
              else if($scope.mapState.directionCurrentFocusField == 'destination'){
                $scope.mapState.directionDestinationPoint = marker.text;
                $scope.mapState.directionDestinationPointMarker = marker;
                $scope.mapState.markerAsDestinationPoint = true;
              }
            }

            $scope.mapState.directionCurrentFocusField = null;
          }

          //handler when user type on direction field
          $scope.directionInputHandler = function(field){
            if(field == 'start'){
              $scope.mapState.markerAsStartPoint = false;
            }
            else if(field == 'destination'){
              $scope.mapState.markerAsDestinationPoint = false;
            }
          }

          //handle the focus event. When user focus on one of the direction text field, this function will be call
          $scope.directionFocusHandler = function(field){
            $scope.mapState.showDirectionList = false;
            

            $timeout(function myFunction() {
              $scope.mapState.showDirectionList = true;
              $scope.mapState.directionCurrentFocusField = field;
            },100);
          }

          //add the button which will allow user to focus into the last marker in the map
          $scope.addFocusButton = function(){
            if($scope.overlayControls.controls.enableFocus == true){
              var map = $scope.getMapObject();
              var focusText = $scope.overlayControls.controls.focusText;

              var svg = angular.element(''
                +'<div>'
                  +'<form class="form-horizontal">'
                    +'<div>'
                      +'<button class="btn btn-success btn-sm" style="margin-top: 5px;" ng-click="focusOnMarkerWithTag(overlayControls.controls.focusTag);">'
                        +'<span class="glyphicon glyphicon-screenshot"></span> {{"'+focusText+'" | i18n}}'
                      +'</button>'
                    +'</div>'
                    +'<div>'
                      +'<button class="btn btn-info btn-sm" style="margin-top: 5px;" ng-click="showDirection();" ng-show="mapState.inDirectionMode == true">'
                        +'<span class="glyphicon glyphicon-random"></span> {{"Show Direction" | i18n}}'
                      +'</button>'
                    +'</div>'
                  +'</form>'
                +'</div>'
              );
              svg[0].index = 1;
              var generateControl = $compile(svg.contents())($scope);
              map.controls[google.maps.ControlPosition.TOP_LEFT].push(generateControl[0]);
            }
          }

          //add the search field
          $scope.addSearchField = function(){
            if($scope.overlayControls.controls.enableSearch == true){
              var map = $scope.getMapObject();
              
              var svg = angular.element(''
                +'<div class="row">'
                  +'<div class="col-md-5">'
                    +'<form class="form-horizontal">'
                      +'<div class="form-group" style="margin-top: 3px;">'
                        +'<div class="col-sm-10" style="width: 300px" ng-show="mapState.inDirectionMode == false">'
                          +'<input type="text" class="form-control form-control-with-icon" ng-model="searchBarValue" ng-enter="searchBarHit();">'
                          +'<span class="icon glyphicon glyphicon-search"></span>'
                        +'</div>'
                        +'<div class="col-sm-10" style="width: 300px" ng-show="overlayControls.controls.enableDirection && mapState.inDirectionMode == false">'
                          +'<div class="angular-direction-box clickable" ng-click="triggerDirectionMode();">'
                            +'<div class="content">'
                              +'Search for direction'
                              +'<a href="javascript:void(0);" style="float: right"><img src="../images/assets/controls/maps/directionIcon.png" alt="" /></a>'
                            +'</div>'
                          +'</div>'
                        +'</div>'

                        +'<div class="col-sm-10" style="width: 300px" ng-show="mapState.inDirectionMode && overlayControls.controls.enableDirection">'
                          +'<div class="angular-direction-form">'
                            +'<div class="widget-directions-left-overlay">'
                            +'</div>'
                            +'<div class="widget-directions-right-overlay">'
                              +'<a href="javascript:void(0);" class="widget-directions-reverse" ng-click="stopDirectionMode();">'
                                +'<img src="../images/assets/controls/maps/closeDirectionIcon.png" alt="" />'
                              +'</a>'
                            +'</div>'
                            +'<div class="widget-directions-searchboxes">'
                              +'<div class="widget-directions-searchbox-container" style="max-height: none; visibility: visible;">'
                                +'<div class="widget-directions-searchbox-handle">'
                                  +'<div class="widget-directions-icon waypoint-bullet"><img src="../images/assets/controls/maps/startPointIcon.png" alt="" /></div>'
                                +'</div>'
                                +'<div class="searchbox">'
                                  +'<table cellspacing="0" cellpadding="0" class="searchboxinput" style="padding: 0px;">'
                                    +'<tbody>'
                                      +'<tr>'
                                        +'<td>'
                                          +'<input autocomplete="off" ng-change="directionInputHandler(\'start\');" ng-model="mapState.directionStartPoint" ng-focus="directionFocusHandler(\'start\')" spellcheck="false" style="border: none; padding: 0px; margin: -0.0625em 0px 0px; height: 1.25em; width: 100%; outline: none;" placeholder=\'{{"Choose starting point from picker, or input the address here..." | i18n}}\'>'
                                        +'</td>'
                                      +'</tr>'
                                    +'</tbody>'
                                  +'</table>'
                                  +'<span class="widget-directions-input-underline"></span>'
                                +'</div>'
                              +'</div>'
                              +'<div class="widget-directions-searchbox-container" style="max-height: none; visibility: visible;">'
                                +'<div class="widget-directions-searchbox-handle">'
                                  +'<div class="widget-directions-icon waypoint-bullet"><img src="../images/assets/controls/maps/destinationIcon.png" alt="" /></div>'
                                +'</div>'
                                +'<div class="searchbox">'
                                  +'<table cellspacing="0" cellpadding="0" class="searchboxinput" style="padding: 0px;">'
                                    +'<tbody>'
                                      +'<tr>'
                                        +'<td>'
                                          +'<input autocomplete="off" ng-change="directionInputHandler(\'destination\');" ng-model="mapState.directionDestinationPoint" ng-focus="directionFocusHandler(\'destination\')" spellcheck="false" style="border: none; padding: 0px; margin: -0.0625em 0px 0px; height: 1.25em; width: 100%; outline: none;" placeholder=\'{{"Choose direction from picker, or input the address here..." | i18n}}\'>'
                                        +'</td>'
                                      +'</tr>'
                                    +'</tbody>'
                                  +'</table>'
                                +'</div>'
                              +'</div>'
                            +'</div>'
                          +'</div>'
                        +'</div>'

                        +'<div class="col-sm-10" style="width: 300px" ng-show="markers.length > 0 && mapState.directionCurrentFocusField && mapState.showDirectionList && mapState.inDirectionMode && overlayControls.controls.enableDirection">'
                          +'<div class="angular-direction-select">'
                            +'<div class="angular-direction-item clickable" data-ng-repeat="marker in markers" ng-click="selectMarkerForDirection(marker);">'
                              +'<img src="{{marker.icon || \'../images/assets/controls/maps/markerIcon.png\'}}" alt="" class="direction-item-icon" width="15px" height="20px" />'
                              +'<div class="direction-item-text">{{marker.text}}</div>'
                            +'</div>'
                          +'</div>'
                        +'</div>'

                      +'</div>'
                    +'</form>'
                  +'</div>'
                +'</div>'
              );
              svg[0].index = 1;
              var generateControl = $compile(svg.contents())($scope);
              map.controls[google.maps.ControlPosition.TOP_CENTER].push(generateControl[0]);
            }
          }

          //init the controller
          $scope.init = function(){
            $timeout(function myFunction() {
              $scope.addFocusButton();
              $scope.addSearchField();
            },1000);
          }

          //event when user search using bar
          $scope.searchBarHit = function(){
            $scope.searchPlaceByName($scope.searchBarValue, true, true, 'directiveMap_searchBar_', null, 'directiveMap_searchBar_');
          }

          //trigger the direction mode
          $scope.triggerDirectionMode = function(){
            $scope.mapState.inDirectionMode = !$scope.mapState.inDirectionMode;
          }

          /* ==================== FUNCTIONS ===================== */
          //hide the messsage box
          $scope.hideFlashMessage = function(){
            $scope.willDisplayMessage = false; 
            $scope.message = null;
          }

          //show the messsage box
          $scope.showMessage = function(message){
            $scope.willDisplayMessage = true; 
            $scope.message = message;
          }

          //get the map object
          $scope.getMapObject = function() {
             return $scope.mapControl.getGMap();
          }

          //when user hit on the marker, open the window
          $scope.onMarkerClicked = function (marker) {
            if($scope.allowClickMarker){
              marker.showWindow = true;
              $scope.$apply();
            }
          };

          $scope.setZoom = function(value){
            $scope.zoom = value;
          }

          //focus the map to selected location
          $scope.focusOnLocation = function(lang, long){
            $scope.center = {
                latitude: lang,
                longitude: long
            };
          };

          //focus on the last marker if there is any
          $scope.focusOnLastMarker = function(){
            if($scope.markers && $scope.markers.length > 0){
              var marker = $scope.markers[$scope.markers.length - 1];
              $scope.focusOnLocation(marker.latitude, marker.longitude);
            }
          }

          //focus on marker with the given tag
          $scope.focusOnMarkerWithTag = function(tag){
            if(!tag){
              console.log("no tag defined");
              return;
            }
            var marker = null;
            var continueLoop = true;
            angular.forEach($scope.markers, function(marker, key) {
              if(continueLoop){
                if(marker.tag == tag){
                  $scope.focusOnLocation(marker.latitude, marker.longitude);
                  continueLoop = false;
                }
              }
            });
          }

          //trigger resize
          $scope.resize = function(){
            //trigger the resize
            var map = $scope.getMapObject();
            google.maps.event.trigger(map, "resize");
          };

          //trigger refresh
          $scope.refresh = function(){
            willRefresh = true;
          }

          //get marker by Id
          $scope.getMarkerById = function(markerId){
            var resultMarker = null;

            angular.forEach($scope.markers, function(marker, key) {

              if(marker.id == markerId){
                resultMarker = marker;
              }
            });

            return resultMarker;
          }

          //update marker data
          $scope.updateMarker = function(markerId, data){
            var marker = $scope.getMarkerById(markerId);
            var index = $scope.markers.indexOf(marker);

            if(marker){
              marker = data;

              return marker;
            }
            else{
              return null;
            }
          }

          $scope.removeAllMarkers = function(){
            dynamicMarkers = [];
            $scope.markers = dynamicMarkers;

            //reload marker
            $scope.reloadMarker();
          };

          $scope.removeAllMarkersWithTag = function(tag){
            var markersWithTag = [];
            angular.forEach($scope.markers, function(marker, key) {
              if(marker.tag == tag){
                markersWithTag.push(marker);
              }
            });

            //remove from markers list
            //var dynamicMarkers = angular.copy($scope.markers);
            angular.forEach(markersWithTag, function(marker, key) {
              var index = $scope.markers.indexOf(marker);
              $scope.markers.splice(index, 1);   
            });

            //$scope.markers = angular.copy(dynamicMarkers);

            //reload marker
            $scope.reloadMarker();
          };

          //remove all polylines with tag
          $scope.removeAllPolilyneWithTag = function(tag){
            var polylinesWithTag = [];
            angular.forEach($scope.polylines, function(line, key) {
              if(line.tag == tag){
                polylinesWithTag.push(line);
              }
            });

            //remove from markers list
            angular.forEach(polylinesWithTag, function(line, key) {
              var index = $scope.polylines.indexOf(line);
              $scope.polylines.splice(index, 1);   
            });
          };

          //add new marker to map
          $scope.addMarker = function(id, lang, long, windowContext, icon, tag){
            var marker = {
                id: id,
                latitude: lang,
                longitude: long,
                showWindow: false,
                text: windowContext,
                icon: (icon)?icon:null,
                tag: (tag)?tag:null,
              };
              
            marker.closeClick = function () {
              marker.showWindow = false;
              $scope.$apply();
            };
            marker.onClicked = function () {
              $scope.onMarkerClicked(marker);
            };

            $scope.markers.push(marker);

            //reload marker
            $scope.reloadMarker();

            return marker;
          };

          $scope.reloadMarker = function(){
            var newMarker = [];
            angular.forEach($scope.markers, function(marker, key) {
              newMarker.push(angular.copy(marker));  
            });
            $scope.markers = newMarker;
            $scope.$apply();
          }

          /* ==================== GOOGLE API FUNCTION ===================== */
          //call google map api to search for path between two direction.
          $scope.getDirection = function(latStart, longStart, latEnd, longEnd, polylineTag, prefixId, polylineSetting, travelingMode){
            if(!latStart || !longStart || !latEnd || !longEnd || !polylineTag){
              $bootbox.alert("Invalid direction request!");
              return;
            }

            $scope.removeAllPolilyneWithTag(polylineTag);

            var directionsService = new google.maps.DirectionsService();

            var directionMode = google.maps.TravelMode.DRIVING;
            if(travelingMode){
              directionMode = travelingMode;
            }

            var startPoint = new google.maps.LatLng(latStart, longStart);
            var endPoint = new google.maps.LatLng(latEnd, longEnd);

            var request = {
                origin: startPoint,
                destination: endPoint,
                travelMode: directionMode
            };

            $scope.displayLoadingBar = true;
            directionsService.route(request, function(response, status) {
              if (status == google.maps.DirectionsStatus.OK) {
                if(response.routes.length > 0){
                  var routeCount = 0;
                  angular.forEach(response.routes, function(route, key) {
                    routeCount++;
                    var decodePath = route.overview_path;

                    var polyline = {
                      id: (prefixId)?(prefixId+routeCount):routeCount,
                      tag: (polylineTag)?polylineTag:'direction',
                      path: [],
                      stroke: {
                          color: (polylineSetting && polylineSetting.lineColor)?polylineSetting.lineColor:'#00b3fd',
                          weight: (polylineSetting && polylineSetting.lineWeight)?polylineSetting.lineWeight:5
                      },
                      editable: (polylineSetting && polylineSetting.editable)?polylineSetting.editable:true,
                      draggable: (polylineSetting && polylineSetting.draggable)?polylineSetting.draggable:false,
                      geodesic: (polylineSetting && polylineSetting.geodesic)?polylineSetting.geodesic:true,
                      visible: (polylineSetting && polylineSetting.visible)?polylineSetting.visible:true,
                      icons: (polylineSetting && polylineSetting.icons)?null:
                      [{
                          icon: {
                              path: null
                          },
                          offset: '25px',
                          repeat: '50px'
                      }],
                    }

                    //add path
                    angular.forEach(decodePath, function(path, key) {
                      var path = {
                        latitude: path.k,
                        longitude: path.B
                      }

                      polyline.path.push(path);
                    });

                    //add line
                    $scope.polylines.push(polyline);
                  });

                  //focus on the start point after done
                  $scope.focusOnLocation(latStart, longStart);
                }
                else{
                  $bootbox.alert("Cannot find route between the selected point. Please make sure you choose the valid points!");
                }
              }
              else{
                $bootbox.alert("Fail to get direction due to error from the request. Please check again!");
              }

              $scope.displayLoadingBar = false;
              $scope.$apply();
            });
          }

          //search only, won't add marker on map
          $scope.searchPlaceLocation = function(search, successHandler){
            if(!search || search == ''){
              return true;
            }
            $scope.displayLoadingBar = true;

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': search}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                //display all result

                var resultLocations = [];
                for(i = 0; i < results.length; i++){
                  var location = {
                    lat: results[i].geometry.location.lat(),
                    lng: results[i].geometry.location.lng(),
                    address: results[i].formatted_address,
                  };

                  resultLocations.push(location);
                }

                if(successHandler){
                    successHandler(resultLocations);
                }

                $scope.displayLoadingBar = false;
                $scope.$apply();

              } else {
                $scope.showMessage('Geocode was not successful for the following reason: ' + status);

                $scope.displayLoadingBar = false;
                $scope.$apply();
              }

              
            });
          }

          //call google api and load places base on the search name
          $scope.searchPlaceByName = function(search, displayMultiPlace, needRemoveOtherMarkers, markerPrefixId, markerIcon, tag, successHandler){
            var prefix = 'marker_search';
            if(markerPrefixId){
              prefix = markerPrefixId;
            }
            var tagMarker = 'marker_search';
            if(tag){
              tagMarker = tag;
            }

            if(!search || search == ''){
              $scope.removeAllMarkersWithTag(tagMarker);
              return;
            }

            $scope.displayLoadingBar = true;
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': search}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                if(displayMultiPlace == true){

                  //display all result
                  if(needRemoveOtherMarkers == true){
                    $scope.removeAllMarkersWithTag(tagMarker);
                  }

                  var resultMarkers = [];
                  for(i = 0; i < results.length; i++){
                    var location = {
                      lat: results[i].geometry.location.lat(),
                      lng: results[i].geometry.location.lng()
                    };

                    if(i==0){
                      $scope.focusOnLocation(location.lat, location.lng);
                    }

                    //display marker
                    var newMarker = $scope.addMarker(prefix + (i + 1), location.lat, location.lng, results[i].formatted_address, (markerIcon)?markerIcon:null, tagMarker);
                    resultMarkers.push(newMarker);
                  }

                  if(successHandler){
                      successHandler(resultMarkers);
                  }
                }
                else{
                  var location = {
                    lat: results[0].geometry.location.lat(),
                    lng: results[0].geometry.location.lng()
                  };
                  $scope.focusOnLocation(location.lat, location.lng);

                  //display marker
                  if(needRemoveOtherMarkers){
                    $scope.removeAllMarkersWithTag(tagMarker);
                  }
                  var newMarker = $scope.addMarker(prefix + (1), location.lat, location.lng, search, (markerIcon)?markerIcon:null, tagMarker);

                  if(successHandler){
                      successHandler([newMarker]);
                  }
                }

                $scope.displayLoadingBar = false;
                $scope.$apply();

              } else {
                $scope.showMessage('Geocode was not successful for the following reason: ' + status);

                $scope.displayLoadingBar = false;
                $scope.$apply();
              }
              
            });
          }

          /* ================== Watcher functions ================== */

          $scope.$watchCollection('markers', function(newMarkers, oldMarkers) {
            _.each($scope.markers, function (marker) {
              marker.closeClick = function () {
                marker.showWindow = false;
                $scope.$apply();
              };
              marker.onClicked = function () {
                $scope.onMarkerClicked(marker);
              };
            });
          });

          /* ==================== DEFINE CONTROLS ===================== */
          if ($scope.control) {
            $scope.control.removeAllMarkers = $scope.removeAllMarkers;
            $scope.control.addMarker = $scope.addMarker;
            $scope.control.refresh = $scope.refresh;
            $scope.control.resize = $scope.resize;
            $scope.control.searchPlaceByName = $scope.searchPlaceByName;
            $scope.control.focusOnLocation = $scope.focusOnLocation;
            $scope.control.focusOnLastMarker = $scope.focusOnLastMarker;
            $scope.control.setZoom = $scope.setZoom;
            $scope.control.reloadMarker = $scope.reloadMarker;
            $scope.control.getMarkerById = $scope.getMarkerById;
            $scope.control.updateMarker = $scope.updateMarker;
          }

          /* ==================== Precall CONTROLS ===================== */
          $timeout(function myFunction() {
            $scope.resize();
          },1000);
        },
        compile: function(tElement, tAttrs) {
            return function($scope, $elem, $attr) {
            }
        }
      };
    }
  ]);
}).call(this);