//check the permission on page before load the page, to see if user have permission to access this page or not. Kick them out if this is illegal access
angular.module('run.accessPermission', []).run(['$rootScope', 'sessionService', 'locationService', 'permissionService', function ($rootScope, sessionService, locationService, permissionService) {
    $rootScope.$on('$routeChangeStart', function (event, currRoute, prevRoute) {
        //check if this page access is configure or not. If now, it is free pass
        if(currRoute.access){
            //check if this page is restrict or not
            if(currRoute.access.restrict){
                //since it is restrict, kick out if user have't login yet
                if(!sessionService.isAuthenticated()){
                    locationService.kickout('This page is restrict for authenticated user only!');
                }

                //check permission require. This condition require all permissions to be satisfy, one permission missing mean you don't have authentication
                if(currRoute.access.permissions && currRoute.access.permissions.length > 0){
                    if(permissionService.havePermissions(currRoute.access.permissions) == false){
                        locationService.errorRedirectWithStatus(true, 403);
                    };
                }

                //check permission require. This condition require at least one permissions to be satisfy, no need to satisfy all
                if(currRoute.access.oneOfPermissions && currRoute.access.oneOfPermissions.length > 0){
                    if(permissionService.haveOneOfPermissions(currRoute.access.oneOfPermissions) == false){
                        locationService.errorRedirectWithStatus(true, 403);
                    };
                }
            }
        }
    });
}]);