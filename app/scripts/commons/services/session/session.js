//hold every page relate data. Like will show navigation, will show breadcrum,....
angular.module('services.session', []).factory('sessionService', function($window, $location){
return	{
	isAuthenticated:	function(token){	
        var token = $window.localStorage.token;
        if(token && typeof token !== 'undefined'){
        	return true;
        }
        else{
        	return false;
        }
	},
	checkAuthenticated:	function(token){	
        var token = $window.localStorage.token;
        if(token && typeof token !== 'undefined'){
        	//nothing happen
        }
        else{
        	$location.path('/signin');
        }
	},

	saveToken:	function(token){	
        $window.localStorage.token = token;
        $window.localStorage.tokenCreateTime = new Date().getTime().toString();
	},
	getToken: function(){	
        return $window.localStorage.token;
	},
	removeToken: function(){	
        delete $window.localStorage.token;
	},

	saveUserData:	function(data){	
        $window.localStorage.userData = data;
	},
	getUserData: function(){	
		try {
            var userData = JSON.parse($window.localStorage.userData);
        	return userData;
        }
        catch (e) {
            console.log("did not receive a valid Json: " + e);
            return null;
        }
	},
	removeUserData: function(){	
        delete $window.localStorage.userData;
	},

	saveName:	function(name){	
        $window.localStorage.name = name;
	},
	getName: function(){	
        return $window.localStorage.name;
	},
	removeName: function(){	
        delete $window.localStorage.name;
	},

	saveTenant:	function(tenant){	
        $window.localStorage.tenant = tenant;
	},
	getTenant: function(){	
        return $window.localStorage.tenant;
	},
	removeTenant: function(){	
        delete $window.localStorage.tenant;
	},

	saveLogo:	function(logo){	
        $window.localStorage.logo = logo;
	},
	getLogo: function(){	
        return $window.localStorage.logo;
	},
	removeLogo: function(){	
        delete $window.localStorage.logo;
	},

	savePermissions:	function(permissions){	
        $window.localStorage.permissions = permissions;
	},
	getPermissions: function(){	
		var permissions = angular.fromJson($window.localStorage.permissions);
		return permissions;
        
	},
	removePermissions: function(){	
        delete $window.localStorage.permissions;
	},
}
});
