angular.module('app.management', [
	'app.mangement.users',
	'app.mangement.userDetails',
    'app.mangement.post',
    'app.mangement.createpost',
    'app.mangement.comment',
    'app.mangement.author',
    'app.mangement.resourceImage',
    'app.mangement.resourceVideo',
]);