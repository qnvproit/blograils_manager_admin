angular.module('app.layout.nav', [])

.controller('NavCtrl', [
    '$scope','permissionService', function($scope, permissionService) {

      $scope.displayDashboard = function() {
        return true;
        return permissionService.canDashboard();
      }

      $scope.displayEmergencies = function() {
        return true;
        return permissionService.canEmergencies(); 
      }

      $scope.displayLookups = function() {
        return true;
        return permissionService.canLookups();
      }

      $scope.displayDatasetup = function() {
        return permissionService.canDatasetup();
      }

      $scope.displayVendormanagement = function() {
        return permissionService.canVendormanagement();
      }

      /***************** Mr.Viet 's Code *********************/

      //administrator/usermanagement
      $scope.displayUserManagement = function(){
        return permissionService.canBrowserUser();
      };

      //administrator/shops manageent
      $scope.displayShopManagement = function(){
        return permissionService.canBrowserShop();
      };

      //administrator/productmanagement and category management
      $scope.displayProductManagement = function(){
        return permissionService.canBrowserProduct();
      };

      //administrator/customermanagement
      $scope.displayCustomerManagement = function(){
        return permissionService.canBrowserCustomer();
      };

      //administrator/sale management
      $scope.displaySaleManagement = function(){
        return permissionService.canBrowserOrder();
      };

      //administrator/events management
      $scope.displayEventsManagement = function(){
        return permissionService.canViewSystemEvent() || permissionService.canViewShopEvent();
      };

      $scope.displayPermissionManagement = function(){
        return permissionService.canBrowserSystemPermission();
      };

      $scope.displayFinancialManagement = function(){
        return permissionService.canAccessSystemFinancial() || permissionService.canViewShopFinancial();
      };
      
    }
]);