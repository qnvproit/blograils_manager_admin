angular.module('service.config.httpProvider', [])

.factory('httpProviderInterceptor', ['$q', 'locationService', function($q, locationService) {  
    function success(response) {
            return response;
    }
    function error(response) {
        var status = response.status;
        if (status == 401) {
            locationService.kickout("Invalid authentication! You need to signin before using this feature!");
            return;
        }
        // otherwise
        return $q.reject(response);
    }
    return function (promise) {
        return promise.then(success, error);
    }
}])

.config(['$httpProvider', function($httpProvider) {
 $httpProvider.responseInterceptors.push(
   'httpProviderInterceptor');
}]);
