angular.module('app.constants', [])
.constant('apiUrl', {
    'listPost' : 'api/postapis/listpost',
    'listTag' : 'api/tagsapis/listtags',
    'createPost' : 'api/postapis/create',
    'delPost': 'api/postapis/delete',
    'suggestPost': 'api/postapis/suggest',

    'listCmt': 'api/cmtsapis/list',
    'detailCmt' : 'api/cmtsapis/detail',
    'setStatusCmt' : "api/cmtsapis/set_status",
    "deleteCmt": "api/cmtsapis/delete",
    'listAuthor': 'api/cmtsapis/author',

    'listImages': 'api/resources/images',
    'listVideos': 'api/resources/videos',
    'uploadImgs': 'api/upload/images',
})