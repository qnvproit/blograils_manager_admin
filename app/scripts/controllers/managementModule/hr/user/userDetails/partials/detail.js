angular.module('app.mangement.userDetails')

/** 
* service hold the configuration of the page
*/

.factory('userDetailsPageConfiguration', function () {
    var configurations = {};

    /* ------------------ Breadcrumbs configuration ------------------ */
    /**
    * Get breadcrumbs configure
    */
    configurations.breadcrumbs = function(userId) {
        return [{
            href: '#/posadmin/hr/users',
            label: 'HR'
        },{
            href: '#/posadmin/hr/users',
            label: 'Staffs'
        },{
            href: '#/posadmin/hr/user/detail/' + userId,
            label: 'Details'
        }];
    };

    /**
    * function to reload breadcrumbs data with new user data
    */
    configurations.reloadBreadcrumbs = function(userData){
        var newConfiguration = configurations.breadcrumbs(userData.user_id);
        newConfiguration[2].label = userData.name;
        return newConfiguration;
    }

    return configurations;
})

/**
* main controller of user detail. All the other controllers will be nested inside this controller
*/
.controller('userDetailsCtrl', [
    '$scope', 
    '$routeParams',
    '$q',
    'userService',
    'userDetailsPageConfiguration',
    'permissionService',
    function($scope, $routeParams, $q, userService, userDetailsPageConfiguration, permissionService) {
    	/* ================== PARAMS ================== */
    	$scope.userId = $routeParams.userId;						//get router parameter
    	$scope.currentUser = null;									//the user object

        $scope.breadcrumbs = userDetailsPageConfiguration.breadcrumbs($scope.userId);

        /* ================== DEFER AND PROMISE ================== */
        /* promise for fetch user detail */
        var fetchUserDefer = $q.defer();
        fetchUserDefer.promise.then(
            function(userModel) {
                $scope.currentUser = userModel;

                //reload page with user data
                $scope.breadcrumbs = userDetailsPageConfiguration.reloadBreadcrumbs($scope.currentUser);
            }, 
            function(data, status, headers, config) {

            }
        );

        /* ================== PAGE EVENT ================== */
        /* the init function, called when page load */
        $scope.pageDidLoad = function(){
            userService.fetchUser($scope.userId, fetchUserDefer.resolve, fetchUserDefer.reject);
        }

    	/* ================== FUNCTIONS ================== */

        /* ================== PAGE BEHAVIOUS ================== */
        /* the init function, called when page load */
        $scope.pageDidLoad();

    }
]);