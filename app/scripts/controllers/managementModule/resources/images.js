angular.module('app.mangement.resourceImage', [])
.controller('ImgCtrl', [
    '$scope', 
    'ajaxService', 
    'apiUrl',
    'sessionService',
    '$modal',
    '$bootbox',
    'logger',
    'FileUploader',
    function($scope, ajaxService, apiUrl, sessionService, $modal, $bootbox, logger, FileUploader) {

        $scope.isBusy = false;

        $scope.breadcrumbs = [
            {
                href: '#/posadmin/hr/users',
                label: 'List Images'
            },
        ];
       

        $scope.uploader = new FileUploader({
            url: apiUrl.uploadImgs,
            method: "POST"

        });

        $scope.uploader.filters.push({
            name: 'imageFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });

        $scope.uploader.onSuccessItem = function(fileItem, response, status, headers) {
            // console.info('onSuccessItem', fileItem, response, status, headers);
            console.log(response);
            $scope.getListImages();
            logger.log(response.message);
            $scope.uploader.removeFromQueue(fileItem);

        };
        $scope.uploader.onErrorItem = function(fileItem, response, status, headers) {
            // console.info('onErrorItem', fileItem, response, status, headers);
            console.log(response);
            $scope.getListImages();
            logger.logError(response.message);
        };

        $scope.showImage = function(url) {
            $scope.modal = $modal.open({
                url: "",
                scope: scope
            })
        }

        $scope.getListImages = function() {
            $scope.isBusy = true;
            ajaxService.request(
                apiUrl.listImages,
                {

                }, function(data) {
                    $scope.isBusy = false;
                    if(data.success) {
                        $scope.response = data.data;

                    } else {
                        logger.logError(data.message);
                    }
                }, function(data) {

                })
        }

        $scope.getListImages();


    }
]);