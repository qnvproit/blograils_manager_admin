describe('Service:HttpProviderInterceptor tests', function () {
    var httpProviderInterceptor;
    var $q;
    var $location;
    var $httpBackend;
    var $http;
    var mockLocationService;

    var kickoutPath = 'kickout';

    beforeEach(function(){  
        module('service.config.httpProvider');

        mockLocationService = {
            //fake the function, return a string as url for login
            kickout: function(value){
                $location.path(kickoutPath);
            }
        };

        module(function ($provide) {
            $provide.value('locationService', mockLocationService);
        });

        inject(function (_httpProviderInterceptor_, _$location_, _$q_, _$httpBackend_, _$http_) { 
            httpProviderInterceptor = _httpProviderInterceptor_;
            $q = _$q_;
            $location = _$location_;
            $httpBackend = _$httpBackend_;
            $http = _$http_;
        })
    });

    it('When request is called but result in 401, make sure the user get redirect to right path', function () {
        spyOn($location, 'path'); 

        $httpBackend.when('GET', '/test').respond(401);
        $http.get('/test');
        $httpBackend.flush();
   
        expect($location.path).toHaveBeenCalledWith(kickoutPath);
    });
});