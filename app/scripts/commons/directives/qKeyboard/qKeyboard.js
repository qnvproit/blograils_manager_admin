angular.module( 'directives.qKeyboard', [] ).directive( 'qKeyboard', [ function() {

	return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
        	
            event.preventDefault();

        });
    };

}] );