//hold the function relate to api. This is where we store all the api of the server
angular.module('services.api', []).factory('apiService', function($location){

	var domain = '';										//domain of the api

	return	{
		//default page
		setDomain:	function(value){	
	        domain = value;
		},
		domain:	function(){	
	        return domain;
		},
		apiUrl: function(name){
			if(angular.equals(name, 'signin')){
				return domain + '/' + 'api/admins/login';
			}
			else if(angular.equals(name, 'signout')){
				return domain + '/' + 'auth/logoutApi';
			}
			else if(angular.equals(name, 'listUser')){
				return domain + '/' + 'user/listUserApi';
			}
			else if(angular.equals(name, 'updateUsersState')){
				return domain + '/' + 'user/changeUsersStateApi';
			}
			else if(angular.equals(name, 'searchUser')){
				return domain + '/' + 'user/findUserByTextApi';
			}
			else if(angular.equals(name, 'removeUsers')){
				return domain + '/' + 'user/removeUsersApi';
			}
			else if(angular.equals(name, 'uploadUserAvatar')){
				return domain + '/' + 'user/uploadAvatarApi';
			}
			else if(angular.equals(name, 'createUser')){
				return domain + '/' + 'user/createUserApi';
			}
			else if(angular.equals(name, 'userDetail')){
				return domain + '/' + 'user/detailApi';
			}
			else if(angular.equals(name, 'updateUser')){
				return domain + '/' + 'user/updateUserApi';
			}
			else if(angular.equals(name, 'searchPermissionGroup')){
				return domain + '/' + 'userPermission/findPermissionGroupByTextApi';
			}
			else{
				return '';
			}
		}
	}
});
