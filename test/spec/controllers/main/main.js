describe('Controllers: Main controller', function () {
    var $scope;
    var controller;
    var $location;
    var mockLocationService;
    var mockPageSettingService;
    var mockSessionService;
    var mockApiService;
    var mockAuthenticationService;

    //data for location service
    var locationServiceData = {
        defaultPath : '',
    }

    //data for pageSettingService
    var pageSettingServiceData = {
        template : '',
        bodyClass : '',
        willShowNav : '',
    }

    //data for session service
    var sessionServiceData = {
        name: 'user name',
        defaultName: 'Guest',
        tenant: 'tenant',
        logo: 'logo',
        avatar: 'avatar',
        defaultAvatar: '/images/assets/default/userAvatar.png',
    }

    //data for api service
    var apiServiceData = {
        domain : '',
    }

    beforeEach(function () {
        module('app.main');

        //mock for location service
        mockLocationService = {
            setDefaultPage: function(value){
                locationServiceData.defaultPath = value;
            },
            defaultRedirect: function(){
                $location.path(locationServiceData.defaultPath);
            }
        };

        //mock for page setting
        mockPageSettingService = {
            template: function(){
                return pageSettingServiceData.template;
            },
            bodyClass: function(){
                return pageSettingServiceData.bodyClass;
            },
            willShowNav: function(){
                return pageSettingServiceData.willShowNav;
            },
        };

        //mock for sessing service
        mockSessionService = {
            getName: function(){
                return sessionServiceData.name;
            },
            getTenant: function(){
                return sessionServiceData.tenant;
            },
            getLogo: function(){
                return sessionServiceData.logo;
            },
            getUserData: function(){
                return {
                    avatar_url: sessionServiceData.avatar,
                };
            }
        };

        //mock for api service
        mockApiService = {
            setDomain: function(value){
                apiServiceData.domain = value;
            },
            domain: function(){
                apiServiceData.domain;
            }
        };

        //mock authentication service
        mockAuthenticationService = {
            signOut: function(successHandler, failureHandler){

            }
        }

        module(function ($provide) {
            $provide.value('locationService', mockLocationService);
            $provide.value('pageSettingService', mockPageSettingService);
            $provide.value('sessionService', mockSessionService);
            $provide.value('apiService', mockApiService);
            $provide.value('authenticationService', mockAuthenticationService);
        });

        inject(function (_$rootScope_, _$controller_, _$location_) {
            $scope = _$rootScope_.$new();

            //setup spy for functiron which is called when controller got init
            spyOn(mockApiService, 'setDomain');

            controller = _$controller_('MainCtrl', {
                '$scope': $scope,
            });

            $location = _$location_;
        });
    });

    it('expect the location default is set and when call the redirect, it go to the default path', function () {
        locationServiceData.defaultPath = 'default_path';
        spyOn($location, 'path');    
        $scope.redirectToHomePage();
        expect($location.path).toHaveBeenCalledWith(locationServiceData.defaultPath);
    });

    it('expect the api service to be called and set the root domain for api', function () {
        expect(mockApiService.setDomain).toHaveBeenCalled();
    });

    it('expect the function used in template definer return right data', function () {
        //template name checking function
        pageSettingServiceData.template = 'template';

        expect($scope.isTemplate(pageSettingServiceData.template)).toEqual(true);
        expect($scope.isTemplate('wrong template name')).toEqual(false);

        //GetBodyclass return set data
        pageSettingServiceData.bodyClass = 'bodyClass';

        expect($scope.getBodyClass()).toEqual(pageSettingServiceData.bodyClass);

        //willShowNav return set data
        pageSettingServiceData.willShowNav = true;

        expect($scope.showNav()).toEqual(pageSettingServiceData.willShowNav);
    });

    it('expect the global data is set as we intended', function () {
        expect($scope.global.name).toEqual(sessionServiceData.name);
        expect($scope.global.tenant).toEqual(sessionServiceData.tenant);
        expect($scope.global.avatar).toEqual(sessionServiceData.avatar);
        expect($scope.global.logo).toEqual(sessionServiceData.logo);
    });
    
});
