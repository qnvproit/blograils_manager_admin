angular.module('directives.dataTable', []).directive('dynamicTable', [
  function() {
    return {
      restrict: 'E',
      scope: {
          config: '=',
          control: '=',
      },
      templateUrl: 'scripts/commons/directives/dataTable/v1.0/tpl/dataTable.html',
      replace: true,
      controller: function($scope, $location, $http, $modal){
          $scope.templatePath = 'scripts/commons/directives/dataTable/v1.0/';

          /* ==================== CONFIGURE PARAMS ===================== */
          $scope.title = $scope.config.title;               //table title
          $scope.icon = $scope.config.icon;                 //table icon
          $scope.token = $scope.config.token;                 //access token for server request
          $scope.fetchRequest = $scope.config.fetchRequest;   //request config
          $scope.itemIdKey = $scope.config.itemIdKey;         //the main key for item
          $scope.rowEditFormName = $scope.config.rowEditFormName;       //name of the form will be used to edit row
          $scope.dateFormat = ($scope.config.dateFormat)?$scope.config.dateFormat:"MMM dd, yyyy";
          $scope.timeFormat = ($scope.config.dateFormat)?$scope.config.timeFormat:"HH:mm:ss";
          $scope.datetimeFormat = ($scope.config.dateFormat)?$scope.config.timeFormat:"MMM dd, yyyy - HH:mm:ss";
          $scope.tableConfig = ($scope.config.tableConfig)?$scope.config.tableConfig:null;

          /* ==================== PARAMS ===================== */
          //store the table limit option
          $scope.tblLimits = [{ "value": 10, "text": "10" }, { "value": 20, "text": "20" }, { "value": 50, "text": "50" }, { "value": 100, "text": "100" }, { "value": -1, "text": "All" }];
          //store the data for table fetch request
          $scope.tblFetchData = {
            accessToken: $scope.token,
            orderBy: ($scope.tableConfig && $scope.tableConfig.orderBy)?$scope.tableConfig.orderBy:null,
            order: ($scope.tableConfig && $scope.tableConfig.order)?$scope.tableConfig.order:"ASC",
            limit: $scope.tblLimits[0].value,
            offset: ($scope.tableConfig && $scope.tableConfig.offset)?$scope.tableConfig.offset:null,
            selects: ($scope.tableConfig && $scope.tableConfig.selects)?$scope.tableConfig.selects:null,
            searchs: ($scope.tableConfig && $scope.tableConfig.searchs)?$scope.tableConfig.searchs:null,
            stringSearch: ($scope.tableConfig && $scope.tableConfig.stringSearch)?$scope.tableConfig.stringSearch:null
          };

          if ($scope.fetchRequest.data) {
            angular.extend($scope.tblFetchData, $scope.fetchRequest.data);
          };

          //store the message data
          $scope.tblRequestResponse = {
            showMessage: false,
            result: false,
            message: '',
            statusCode: '',
          };

          $scope.advanceSearchData = {};

          //store the loading data
          $scope.tblBusyLoad = null;

          //store the items
          $scope.tblItems = [];

          $scope.selectList = [];                 //array contain select list
          $scope.selectAll = false;

          //store the current paging
          $scope.tblCurrentPage = 1;
          $scope.tblTotalItems = 0;
          $scope.tblMaxPageSize = 4;

          //store the loading data
          $scope.tblLoader = null;

          //the modal to display advance search
          $scope.advanceSearchModal = null;

          /* ==================== GET,SET FUNCTIONS ===================== */
          //get the path for cell template
          $scope.getCellTemplatePath = function(cellTemplateName){
            return $scope.templatePath + 'tpl/cell/' + cellTemplateName;
          }

          //parse the bindConfig of the field to get the condition for display, disable. Pass type to select which type we need to parse(disable, display)
          $scope.parseFieldBindConfig = function(config, type){
            if(!config){
              return true;
            }
            var result = true;
            var continueLoop = true;
            angular.forEach(config, function(item, key) {
              if(continueLoop && item.type == type){
                if(item.compareType == 'equal'){

                  result = result && (item.targetValue == $scope.advanceSearchData[item.bindModel].data);
                  if(result == false){
                    continueLoop = false;
                  }
                }
              }
            });
            return result;
          }

          //convert string to int
          $scope.toInt = function(value){
            return parseInt(value);
          }

          //get the item base on the model. normally it is item[model], but if model have more than 1 level, for example customer.name, we need to parse it
          $scope.parseItemModel = function(item, model){
            if(!item){
              return '';
            }

            var modelArray = model.split('.');
            if(modelArray.length > 1){
              var firstLevel = item;
              var isNull = false;
              angular.forEach(modelArray, function(arrayItem, key) {
                //if a level is null, stop
                if(!isNull){
                  firstLevel = firstLevel[arrayItem];
                  if(!firstLevel){
                    isNull = true;
                  }
                }
              });

              //is empty
              if(isNull){
                return '';
              }
              //not empty
              return firstLevel;
            }
            else if(modelArray.length == 1){
              return item[model];
            }
            else{
              return '';
            }
          }

          //get the sort model incase we want to set model to another one
          $scope.getSortModel = function(item){
            if(item.sortModel){
              return item.sortModel;
            }
            else if(item.model){
              return item.model;
            }
            else{
              return null;
            }
          }

          //each cell have a display condition, which will display the selected value when the condition meet. For example, if total = 0, we can display "empty" instead
          $scope.getCellDisplayWithCondition = function(value, conditions){
            var returnValue = value;
            angular.forEach(conditions, function(condition, key) {
              if(condition.value == value){
                returnValue = condition.display;
              }
            });

            return returnValue;
          }

          /* ==================== FUNCTIONS ===================== */
          //check if is array
          $scope.isArray = function(item){
            return angular.isArray(item);
          }

          //set title
          $scope.setTitle = function(title){
            $scope.title = title;
          }

          //reload the fetch data
          $scope.reloadFetchRequestData = function(){
            angular.forEach($scope.fetchRequest.data, function(value, key) {
              $scope.tblFetchData[key] = value;
            });
          }
          
          //print string with predefine format
          $scope.printString = function(string, params){
            var newString = string;
            var counter = 0;
            angular.forEach(params, function(param, key) {
                newString = string.replace("{"+counter+"}", param);
                counter++;
              });
            return newString;
          };

          //common function when user call custom function
          $scope.excuteFunctionCustom = function(clickFunction){
            if(clickFunction != null && clickFunction.type=="normal"){
              clickFunction.function();
            }
            else if(clickFunction != null && clickFunction.type=="serverRequestForSelected"){
              clickFunction.function($scope.tblItems, $scope.selectList, function(config){
                var requestData = config.requestData;
                var url = config.url;
                var successCallback = config.successCallback;
                var failCallback = config.failCallback;
                var needReset = config.needReset;
                var needResetSelected = config.needResetSelected;

                $scope.tblLoader = $http.post(url, JSON.stringify(requestData), {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined},
                })
                .success(function(data, status, headers, config) {
                  if(data.error == 1){
                    $scope.requestSuccessWithErrorHandler(data, status, headers, config);

                    if(failCallback != null){
                      failCallback(data, status, headers, config);
                    }
                  }
                  else{
                    $scope.requestSuccessHandler(data, status, headers, config);
                    //get the user data
                    if(successCallback != null){
                      var itemsData = successCallback(data, status, headers, config);
                      if(needReset){
                        $scope.fetchTblData();
                      }
                      else{
                        $scope.tblItems = itemsData.items; 
                        $scope.tblTotalItems = itemsData.total; 
                      }

                      if(needResetSelected){
                        $scope.selectList = [];
                      }
                    }
                  }
                })
                .error(function(data, status, headers, config) {
                    $scope.requestFailHandler(data, status);

                    if(failCallback != null){
                      failCallback(data, status, headers, config);
                    }
                });
              });
            }
          }

          //custom function call when user want custom cell data
          $scope.excuteCustomCellFunction = function(column, item){
            if(column.customDataFunction != null){
              return column.customDataFunction.function(column, item);
            }

            return null;
          };

          //common function when user tap on a cell
          $scope.excuteFunctionOnSelected = function(column, item){
            if(column.enableClick && column.clickFunction != null && column.clickFunction.type=="normal"){
              column.clickFunction.function(column, item);
            }
            else if(column.enableClick && column.clickFunction != null && column.clickFunction.type=="serverRequest"){
              column.clickFunction.function(column, item, function(config){
                var requestData = config.requestData;
                var url = config.url;
                var successCallback = config.successCallback;
                var failCallback = config.failCallback;
                var needReset = config.needReset;

                $scope.tblLoader = $http.post(url, JSON.stringify(requestData), {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined},
                })
                .success(function(data, status, headers, config) {
                  if(data.error == 1){
                    $scope.requestSuccessWithErrorHandler(data, status, headers, config);

                    if(failCallback != null){
                      failCallback(data, status, headers, config);
                    }
                  }
                  else{
                    $scope.requestSuccessHandler(data, status, headers, config);
                    //get the user data
                    if(successCallback != null){
                      var itemsData = successCallback(data, status, headers, config);
                      if(needReset){
                        $scope.fetchTblData();
                      }
                      else{
                        $scope.tblItems = itemsData.items; 
                        $scope.tblTotalItems = itemsData.total; 
                      }
                    }
                  }
                })
                .error(function(data, status, headers, config) {
                  $scope.requestFailHandler(data, status);

                  if(failCallback != null){
                    failCallback(data, status, headers, config);
                  }
                });

              });
            }
          };

          $scope.hideFlashMessage = function(){
            $scope.tblRequestResponse.showMessage = false;
          }

          $scope.showTblAlert = function(message) {
            $scope.tblRequestResponse.showMessage = true;
            $scope.tblRequestResponse.message = message;
          };

          $scope.hideTblAlert = function() {
            $scope.tblRequestResponse.showMessage = false;
            $scope.tblRequestResponse.message = '';
          };

          $scope.orderBy = function(orderBy, order){
            $scope.tblFetchData.orderBy = orderBy;
            $scope.tblFetchData.order = order;

            $scope.fetchTblData();
          }

          $scope.limitChanged = function() {
            $scope.fetchTblData();
          };

          $scope.setPage = function (pageNo) {
            $scope.tblCurrentPage = pageNo;
            $scope.tblFetchData.offset = ($scope.tblCurrentPage - 1) * $scope.tblFetchData.limit;

            $scope.fetchTblData();
          };

          //user toggle the select item, if in selected list, remove it. Other wise, add it
          $scope.toggleSelection = function(item){
            var idx = $scope.selectList.indexOf(item);

              // is currently selected
              if (idx > -1) {
                $scope.selectList.splice(idx, 1);
              }

              // is newly selected
              else {
                $scope.selectList.push(item);
              }
          };

          $scope.isCheckedAll = function(){
            var totalItems = $scope.tblItems.length;
            var totalSelected =  $scope.selectList.length;

            if(totalItems == 0 || totalSelected == 0){
              return false;
            }

            return (totalItems == totalSelected);
          };

          //when user hit the select all checkbox
          $scope.clickSelectAllItems = function(){
            if(!$scope.isCheckedAll()){
              $scope.selectList = [];
              angular.forEach($scope.tblItems, function(item, key) {
                $scope.selectList.push(item[$scope.itemIdKey]);
              });
            }
            else{
              $scope.selectList = [];
            }

            //force reload list
            $scope.tblItems = angular.copy($scope.tblItems);
          };

          //handler when request success but fail
          $scope.requestSuccessWithErrorHandler = function(data, status, headers, config){
            if(data.error == 1){
              $scope.tblRequestResponse.result = false;
              $scope.tblRequestResponse.statusCode = status;
              $scope.showTblAlert(data.message);
            }
          };

          $scope.requestFailHandler = function(data, status){
            $scope.tblRequestResponse.result = false;
            $scope.tblRequestResponse.statusCode = status;
            $scope.showTblAlert(data.message);
          };

          $scope.requestSuccessHandler = function(data, status, headers, config){
            if(data.error != 1){
              $scope.tblRequestResponse.result = true;
              $scope.hideTblAlert();
            }
          };

          //open modal view for search
          $scope.showAdvanceSearch = function(){
            $scope.advanceSearchModal = $modal.open({
              templateUrl: $scope.templatePath + 'tpl/search/advanceSearch.html',
              scope: $scope,
            });
          }

          //handle when click search modal buttons
          $scope.searchModalSearchClick = function(){
            $scope.setAdvanceSearch(false);

            $scope.fetchTblData();
            //$scope.advanceSearchModal.dismiss();
          }

          //handle when click reset modal buttons
          $scope.searchModalResetClick = function(){
            $scope.setAdvanceSearch(true);
            $scope.fetchTblData();

            //$scope.advanceSearchModal.dismiss();
          }

          //apply or remove the advance search data
          $scope.setAdvanceSearch = function(isReset){
            if(isReset){
              $scope.tblFetchData.searchs = null;
              angular.forEach($scope.advanceSearchData, function(value, key) {
                if(value.data && value.data != ""){
                  value.data = value.defaultValue;
                }
              });
            }
            else{
              var searchData = [];
              angular.forEach($scope.advanceSearchData, function(value, key) {
                if(value.data && value.data != ""){

                  //check if key need to protect by `` or not
                  var keyParts = key.split('.');
                  var queryKey = "`" + key + "` ";
                  if(keyParts.length > 1){
                    queryKey = key + " ";
                  }

                  if(value.type == 'like'){
                    var searchExpress = {
                      express: {
                        type: "like",
                        field: queryKey,
                        value: value.data
                      },
                      connect: "AND",
                      isHaving: (value.queryType == 'having')?true:false,
                    };
                    searchData.push(searchExpress);
                  }
                  else if(value.type == 'equal'){
                    var searchExpress = {
                      express: {
                        type: "=",
                        field: queryKey,
                        value: value.data
                      },
                      connect: "AND",
                      isHaving: (value.queryType == 'having')?true:false,
                    };
                    searchData.push(searchExpress);
                  }
                  else if(value.type == 'checknull'){
                    if(value.data){
                      if(value.data == 'notnull'){
                        var searchExpress = {
                          express: {
                            type: "is not null",
                            field: queryKey,
                          },
                          connect: "AND",
                          isHaving: (value.queryType == 'having')?true:false,
                        };
                      }
                      else{
                        var searchExpress = {
                          express: {
                            type: "is null",
                            field: queryKey,
                          },
                          connect: "AND",
                          isHaving: (value.queryType == 'having')?true:false,
                        };
                      }
                      searchData.push(searchExpress);
                    }
                  }
                  else if(value.type == 'daterange'){
                    var searchExpress = {
                      express: {
                        type: "range",
                        field: queryKey,
                        valueBottom: value.data.startDate,
                        valueTop: value.data.endDate,
                      },
                      connect: "AND",
                      isHaving: (value.queryType == 'having')?true:false,
                    };
                    searchData.push(searchExpress);
                  }
                  else if(value.type == 'multiSelect2'){
                    if(value.data.length > 0){
                      var keyArrays = [];
                      angular.forEach(value.data, function(inItem, inKey) {
                        keyArrays.push(inItem[value.itemId]);
                      });

                      var searchExpress = {
                        express: {
                          type: "in",
                          field: queryKey,
                          value: keyArrays
                        },
                        connect: "AND",
                        isHaving: (value.queryType == 'having')?true:false,
                      };
                      searchData.push(searchExpress);
                    }
                  }
                  else if(value.type == 'multiSelect2ContainAll'){
                    if(value.data.length > 0){
                      var keyArrays = [];
                      angular.forEach(value.data, function(inItem, inKey) {
                        keyArrays.push(inItem[value.itemId]);
                      });

                      var searchExpress = {
                        express: {
                          type: "containLikeAll",
                          field: queryKey,
                          value: keyArrays
                        },
                        connect: "AND",
                        isHaving: (value.queryType == 'having')?true:false,
                      };
                      searchData.push(searchExpress);
                    }
                  }
                  else if(value.type == 'numberrange'){
                    if(value.data.bottomScope && value.data.topScope){
                      var searchExpress = {
                        express: {
                          type: "range",
                          field: queryKey,
                          valueBottom: value.data.bottomScope,
                          valueTop: value.data.topScope,
                        },
                        connect: "AND",
                        isHaving: (value.queryType == 'having')?true:false,
                      };
                      searchData.push(searchExpress);
                    }
                    else if(value.data.bottomScope && !value.data.topScope){
                      var searchExpress = {
                        express: "(" + queryKey + ">= " + value.data.bottomScope + ")",
                        express: {
                          type: ">=",
                          field: queryKey,
                          value: value.data.bottomScope,
                        },
                        connect: "AND",
                        isHaving: (value.queryType == 'having')?true:false,
                      };
                      searchData.push(searchExpress);
                    }
                    else if(!value.data.bottomScope && value.data.topScope){
                      var searchExpress = {
                        express: {
                          type: "<=",
                          field: queryKey,
                          value: value.data.topScope,
                        },
                        connect: "AND",
                        isHaving: (value.queryType == 'having')?true:false,
                      };
                      searchData.push(searchExpress);
                    }
                  }
                }
              });
              $scope.tblFetchData.searchs = searchData;
            }
          }

          /* ================== Watcher functions ================== */

          $scope.$watch('selectList.length', function() {
            if($scope.isCheckedAll()){
              $scope.selectAll = true;
            }
            else{
              $scope.selectAll = false;
            }
          });

          $scope.$watch('$scope.config.searchConfig', function() {
            $scope.advanceSearchData = {};

            angular.forEach($scope.config.searchConfig, function(item, key) {
              if(item.type == "text"){
                $scope.advanceSearchData[item.model] = {
                  type: item.searchType,
                  queryType: item.queryType,
                  data: item.value,
                  defaultValue: item.defaultValue,
                };
              }
              else if(item.type == "select"){
                $scope.advanceSearchData[item.model] = {
                  type: item.searchType,
                  queryType: item.queryType,
                  data: item.value,
                  defaultValue: item.defaultValue,
                };
              }
              else if(item.type == "daterange"){
                $scope.advanceSearchData[item.model] = {
                  type: item.searchType,
                  queryType: item.queryType,
                  data: item.value,
                  defaultValue: item.defaultValue,
                };
              }
              else if(item.type == "numberrange"){
                $scope.advanceSearchData[item.model] = {
                  type: item.searchType,
                  queryType: item.queryType,
                  data: {
                    bottomScope: null,
                    topScope: null,
                  },
                  defaultValue: item.defaultValue,
                };
              }
              else if(item.type == "select2"){
                $scope.advanceSearchData[item.model] = {
                  type: item.searchType,
                  queryType: item.queryType,
                  data: item.value,
                  defaultValue: item.defaultValue,
                  itemId: item.itemId,
                };
              }
                
            });

          });

          /* ================== Server functions ================== */
          //call the fetch table list api
          $scope.fetchTblData = function() {
            $scope.reloadFetchRequestData();

            $scope.tblLoader = $http.post($scope.fetchRequest.url, JSON.stringify($scope.tblFetchData), {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined},
            })
            .success(function(data, status, headers, config) {
              if(data.error == 1){
                $scope.requestSuccessWithErrorHandler(data, status, headers, config);

                if($scope.fetchRequest.failCallback != null){
                  $scope.fetchRequest.failCallback(data, status, headers, config);
                }
              }
              else{
                $scope.requestSuccessHandler(data, status, headers, config);

                //get the user data
                if($scope.fetchRequest.successCallback != null){
                  var itemsData = $scope.fetchRequest.successCallback(data, status, headers, config);
                  $scope.tblItems = itemsData.items; 
                  $scope.tblTotalItems = itemsData.total; 
                }
              }
            })
            .error(function(data, status, headers, config) {
              $scope.requestFailHandler(data, status);

              if($scope.fetchRequest.failCallback != null){
                $scope.fetchRequest.failCallback(data, status, null, null);
              }
            });
          };

          /* ==================== DEFINE CONTROLS ===================== */
          if ($scope.control) {
            $scope.control.refresh = $scope.fetchTblData;
            $scope.control.filterData = $scope.tblFetchData;
            $scope.control.setTitle = $scope.setTitle;
          }

          /* ==================== PRECALL ===================== */
          //preload the table for the first time
          $scope.fetchTblData();
      },
      compile: function(tElement, tAttrs) {
          return function($scope, $elem, $attr) {
              
          }
      }
    };
  }
]);
