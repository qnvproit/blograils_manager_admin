describe('Run:pageConfig tests', function () {
    var $rootScope;
    var mockPageSettingService;

    //params of mockPageSettingService
    var bodyClass = '';
    var showNav = true;
    var bodyStyle = 'template';

    beforeEach(function(){  
        module('run.pageConfig');

        //we need to fake the page setting service since this is the custom service.
        mockPageSettingService = {
            setBodyClass: function (value) {
                bodyClass = value;
            },
            setBodyClassDefault: function (){
                bodyClass = 'default';
            },

            setShowNav: function (value) {
                showNav = value;
            },

            setTemplate: function (value){
                bodyStyle = value;
            },
            setTemplateDefault: function (){
                bodyStyle = 'default';
            },
        };

        module(function ($provide) {
            $provide.value('pageSettingService', mockPageSettingService);
        });

        inject(function (_$rootScope_) { 
            $rootScope = _$rootScope_;
        })
    });

    it('When routeChangeStart called, expect the page setting is set base on the given setting', function () {
        //in this case, all the setting is set
        var currRoute = {
            bodyClass : 'test',
            showNav : true,
            bodyStyle : 'template',
        }

        var prevRoute = {

        }

        //make sure all the setting is set with right value
        $rootScope.$broadcast("$routeChangeStart", currRoute, prevRoute);
        expect(bodyClass).toEqual(currRoute.bodyClass);
        expect(showNav).toEqual(currRoute.showNav);
        expect(bodyStyle).toEqual(currRoute.bodyStyle);

        //in this case nothing is set, make sure it have default value
        //in this case, all the setting is set
        var currRoute = {
        }

        $rootScope.$broadcast("$routeChangeStart", currRoute, prevRoute);
        expect(bodyClass).toEqual('default');
        expect(showNav).toEqual(false);
        expect(bodyStyle).toEqual('default');

    });
});