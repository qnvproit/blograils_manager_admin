//hold every page relate data. Like will show navigation, will show breadcrum,....
angular.module('services.pageSetting', []).factory('pageSettingService', [function () {
	var pageSettingService = {};

	var willShowNav = true;
	var bodyClass = 'body-home';
	var template = 'pos';

	//get, set the flag of will show nav
	pageSettingService.willShowNav = function() {
	    return willShowNav;
	};

	pageSettingService.setShowNav = function(value) {
	    willShowNav = value;
	};

	//get, set the body class
	pageSettingService.bodyClass = function() {
	    return bodyClass;
	};

	pageSettingService.setBodyClass = function(value) {
	    bodyClass = value;
	};

	pageSettingService.setBodyClassDefault = function() {
	    bodyClass = 'body-home';
	};

	//get, set the body template
	pageSettingService.template = function() {
	    return template;
	};

	pageSettingService.setTemplate = function(value) {
	    template = value;
	};

	pageSettingService.setTemplateDefault = function() {
	    template = 'pos';
	};

	return pageSettingService;
}]);
