describe('Run:accessPermission tests', function () {
    var $rootScope;
    var $location;

    var mockSessionService;
    var mockLocationService;
    var mockPermissionService;

    var kickoutPath = 'kickout';
    var path403 = '/403';

    //param for session service
    var sessionServiceParams = {
        isAuthenticated : false,
        havePermissions : false,
        haveOnePermission : false,
    }

    beforeEach(function(){  
        module('run.accessPermission');

        //we need to fake the page setting service since this is the custom service.
        mockSessionService = {
            isAuthenticated: function(){
                return sessionServiceParams.isAuthenticated;
            },
        };

        //mock for location service
        mockLocationService = {
            kickout: function(){
                $location.path(kickoutPath);
            },
            errorRedirectWithStatus: function(saveHistory, path){
                $location.path(path403);
            }
        };

        //mock permission service
        mockPermissionService = {
            havePermissions: function(){
                return sessionServiceParams.havePermissions;
            },
            haveOneOfPermissions: function(){
                return sessionServiceParams.haveOnePermission;
            },
        };

        module(function ($provide) {
            $provide.value('sessionService', mockSessionService);
            $provide.value('locationService', mockLocationService);
            $provide.value('permissionService', mockPermissionService);
        });

        inject(function (_$rootScope_, _$location_) { 
            $rootScope = _$rootScope_;
            $location = _$location_;
        })
    });

    it('When page require restrict, but user do not have authentication, kickout is expect', function () {
        //in this case, restrict is true, but authentication is invalid, so expect user got kickout
        var currRoute = {
            access: {
                restrict: true
            }
        }

        var prevRoute = {

        }

        //invalid authentication
        sessionServiceParams.isAuthenticated = false;

        //make sure all the setting is set with right value
        spyOn($location, 'path');    
        $rootScope.$broadcast("$routeChangeStart", currRoute, prevRoute);
        expect($location.path).toHaveBeenCalledWith(kickoutPath);

    });

    it('When page require restrict, and user have authentication, but lack all permission this page require. Expect kick to 403 page', function () {
        //in this case, restrict is true, but authentication is invalid, so expect user got kickout
        var currRoute = {
            access: {
                restrict: true,
                permissions: ['permission']
            }
        }

        var prevRoute = {

        }

        //invalid authentication
        sessionServiceParams.isAuthenticated = true;
        sessionServiceParams.havePermissions = false;

        //make sure all the setting is set with right value
        spyOn($location, 'path');    
        $rootScope.$broadcast("$routeChangeStart", currRoute, prevRoute);
        expect($location.path).toHaveBeenCalledWith(path403);

    });

    it('When page require restrict, and user have authentication, but dont have even one permission require(only one is enough). Expect kick to 403 page', function () {
        //in this case, restrict is true, but authentication is invalid, so expect user got kickout
        var currRoute = {
            access: {
                restrict: true,
                oneOfPermissions: ['permission']
            }
        }

        var prevRoute = {

        }

        //invalid authentication
        sessionServiceParams.isAuthenticated = true;
        sessionServiceParams.havePermissions = true;
        sessionServiceParams.haveOnePermission = false;

        //make sure all the setting is set with right value
        spyOn($location, 'path');    
        $rootScope.$broadcast("$routeChangeStart", currRoute, prevRoute);
        expect($location.path).toHaveBeenCalledWith(path403);

    });
});