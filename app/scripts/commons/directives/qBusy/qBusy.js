angular.module( 'directives.qBusy', [] ).directive( 'qBusy', [ function() {

	return {
		restrict: 'E',
		replace: false,
		templateUrl: "scripts/commons/directives/qBusy/template/qBusy.html",
		scope: {

		}
	}
}])