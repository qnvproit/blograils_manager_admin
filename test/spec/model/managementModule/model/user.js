describe('Model:user tests', function () {
    var userModel;
    var mockSessionService;

    beforeEach(function(){  
        module('app.model.user');

        mockSessionService = {
            getUserData: function(){
                return {user_id : 10};
            },
        };

        module(function ($provide) {
            $provide.value('sessionService', mockSessionService);
        });

        inject(function (_userModel_) { 
            userModel = _userModel_;
        })
    });

    it('The data should be set when called', function () {
        var data = {
            user_id: 'user_id',
            password: 'password',
            name: 'name',
            email: 'email',
            create_date: 'create_date',
            phone: 'phone',
            address: 'address',
            avatar: 'avatar',
            avatar_url: 'avatar_url',
            roles: 'roles',
            active: 'active',   
        };

        var user = new userModel(data);
        expect(user.user_id).toEqual(data.user_id);
        expect(user.password).toEqual(data.password);
        expect(user.name).toEqual(data.name);
        expect(user.email).toEqual(data.email);
        expect(user.create_date).toEqual(data.create_date);
        expect(user.phone).toEqual(data.phone);
        expect(user.address).toEqual(data.address);
        expect(user.avatar).toEqual(data.avatar);
        expect(user.avatar_url).toEqual(data.avatar_url);
        expect(user.roles).toEqual(data.roles);
        expect(user.active).toEqual(data.active);
    });

    it('If the user id of the return user is equal to the one return by session, isCurrentUser have to return true. Otherwise, return false', function () {
        var data = {
            user_id: 10,  
        };

        var user = new userModel(data);
        expect(user.isCurrentUser()).toEqual(true);

        var dataFail = {
            user_id: 11,  
        };

        var user2 = new userModel(dataFail);
        expect(user2.isCurrentUser()).toEqual(false);
    });
});