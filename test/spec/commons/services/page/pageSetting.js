describe('Service:pageSetting get/set properties tests', function () {
	var pageSettingService;
	beforeEach(module('services.pageSetting'));

	beforeEach(inject(function (_pageSettingService_) { 
		pageSettingService = _pageSettingService_;
	}));

	it('The Get/Set function work as expert', function () {
		//check will show nav properties
    	var willShowNav = false;

    	pageSettingService.setShowNav(willShowNav);
    	expect(pageSettingService.willShowNav()).toEqual(willShowNav);

    	//check bodyClass properties
    	var bodyClass = false;

    	pageSettingService.setBodyClass(bodyClass);
    	expect(pageSettingService.bodyClass()).toEqual(bodyClass);

    	//check template properties
    	var template = false;

    	pageSettingService.setTemplate(template);
    	expect(pageSettingService.template()).toEqual(template);
	});
});