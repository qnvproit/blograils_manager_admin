angular.module('app.mangement.users', [])

/* ========================================== service hold the configuration of the page ========================================== */
.factory('usersConfiguration', function ($q, logger, apiService, permissionService, sessionService, logger, $location, localize, $bootbox, $modal) {
	var configurations = {};

	/* ------------------ Breadcrumbs configuration ------------------ */
	configurations.breadcrumbs = function() {
	    return [{
	        href: '#/posadmin/hr/users',
	        label: 'HR'
	    },{
	        href: '#/posadmin/hr/users',
	        label: 'Staffs'
	    }];
	};

	/* ------------------ User Table configuration ------------------ */
	configurations.tblStaffsConfig = function($scope){
		return {
          	title: "Staffs List",
          	icon: 'fa fa-users',
          	itemIdKey: "user_id",

          	token: sessionService.getToken(),

          	//configure the request to get list
          	fetchRequest:{
	            url: apiService.apiUrl('listUser'),
	            successCallback: function(data, status, headers, config){
                    var fetchData = angular.fromJson(data.data);
                    return {
                      items: fetchData.data,
                      total: fetchData.total,
                    };
	            },
	            failCallback: function(data, status, headers, config){
	                  
	            },
        	},
	        //configure the colums
	        columnConfig: {
	            enableRowCheck : true,
	            enableRowControls: true,
	            columns: [
	              	{
		                label: localize.getLocalizedString("Name"),
		                model: 'name',
		                sortModel: 't.name',
		                enableOrder: true,
		                template: "cellText.html",
		                enableClick: true,
		                clickFunction: {
		                  	type: 'normal',
		                  	function: function(column, item){
		                    	$location.path('/administration/user/detail/'+item.user_id);
		                  	}
		                },
	              	},

	              	{
		                label: localize.getLocalizedString("Email"),
		                model: 'email',
		                enableOrder: true,
		                template: "cellMail.html",
	              	},

	              	{
		                label: localize.getLocalizedString("State"),
		                model: 'active',
		                sortModel: 't.active',
		                enableOrder: true,
		                template: "cellState.html",

		                enableClick: permissionService.canEditUser(),
		                clickFunction: {
		                  	type: 'serverRequest',
		                  	function: function(column, item, requestFunction){
			                    requestFunction({
			                      	requestData : {
			                        	accessToken: sessionService.getToken(),
			                        	selectIds: [item.user_id],
			                        	state: !(item.active == 1),
			                      	},
			                      	url: apiService.apiUrl('updateUsersState'),
			                      	needReset: true,
			                      	successCallback: function(data, status, headers, config){
			                        	logger.logSuccess(localize.getLocalizedString("Successfully update selected staff"));
			                      	},
			                      	failCallback: function(data, status, headers, config){

			                      	},
			                    });
		                  	}
		                  
		                },
	              	}
	            ],

	            rowControls: [
	              	{
		                type: 'button',
		                class: 'btn-primary',
		                icon: 'fa fa-eye',
		                tooltip: localize.getLocalizedString("View Detail"),
		                enableClick: true,
		                clickFunction: {
		                  	type: 'normal',
		                  	function: function(column, item){
		                    	$location.path('/posadmin/hr/user/detail/'+item.user_id);
		                  	}
		                },
	              	},
	              	//change user avatar
	                {
	                  	type: 'button',
	                  	class: 'btn-primary',
	                  	icon: 'fa fa-camera',
	                  	tooltip: localize.getLocalizedString("Change Avatar"),
	                  	enableClick: true,
	                  	clickFunction: {
	                    	type: 'normal',
	                    	function: function(column, item){
		                      	var imageUrl = null;
		                      	if(item.avatar_url && item.avatar_url != ''){
		                        	imageUrl = item.avatar_url;
		                      	}
		                      
		                      	$scope.avatarUploaderControls.open(item.user_id, imageUrl);
	                    	}
	                  	},
	                  	ngShow: function(){
	                  		//only show up if have permission
		                  	return permissionService.canEditUser();
		                },
	                },
	              	//remove selected user
	                {
	                  	type: 'button',
	                  	class: 'btn-danger',
	                  	icon: 'fa fa-trash-o',
	                  	tooltip: localize.getLocalizedString("Delete User"),
	                  	enableClick: true,
	                  	clickFunction: {
	                    	type: 'serverRequest',
	                    	function: function(column, item, requestFunction){
		                      	$bootbox.confirm(localize.getLocalizedString("Are you sure you want to remove this item?"), function(result) {
		                        	if(result){
		                          		requestFunction({
			                            	requestData : {
			                              		accessToken: sessionService.getToken(),
			                              		removeIds: [item.user_id],
			                            	},
			                            	url: apiService.apiUrl('removeUsers'),
			                            	needReset: true,
			                            	successCallback: function(data, status, headers, config){
			                              		logger.logSuccess(localize.getLocalizedString("Successfully remove selected user"));
			                            	},
			                            	failCallback: function(data, status, headers, config){

			                            	},
		                          		});
		                        	}
		                      	});
		                    }
	                  	},
	                  	ngShow: function(){
	              			//only show up if have permission
	                  		return permissionService.canRemoveUser();
	                	},
	                },

	            ]
	        },

	        //configure the button on top of the table
	        tableTools: [
	            {
	              	type: 'tools',
	              	label: localize.getLocalizedString("Actions"),
	              	items:[
	                {
	                  	type: 'tool',
	                  	label: localize.getLocalizedString("Active All Selected"),
	                  	clickFunction: {
		                    type: 'serverRequestForSelected',
		                    function: function(items, selectedItems, requestFunction){

		                      	$bootbox.confirm(localize.getLocalizedString("Are you sure you want to active all selected items?"), function(result) {
			                        if(result){
			                          	requestFunction({
				                            requestData : {
				                              	accessToken: sessionService.getToken(),
				                              	selectIds: selectedItems,
				                              	state: true,
				                            },
				                            url: apiService.apiUrl('updateUsersState'),
				                            needReset: true,
				                            needResetSelected: true,
				                            successCallback: function(data, status, headers, config){
				                              	logger.logSuccess(localize.getLocalizedString("Successfully updated selected users"));
				                            },
				                            failCallback: function(data, status, headers, config){
				                                    
				                            },
				                        });
			                        }
		                      	});

		                    }
	                  	}
	                },
	                {
	                  	type: 'tool',
	                  	label: localize.getLocalizedString("Deactive All Selected"),
	                  	clickFunction: {
		                    type: 'serverRequestForSelected',
		                    function: function(items, selectedItems, requestFunction){

			                    $bootbox.confirm(localize.getLocalizedString("Are you sure you want to deactive all selected items?"), function(result) {
			                        if(result){
				                        requestFunction({
				                            requestData : {
				                              	accessToken: sessionService.getToken(),
				                              	selectIds: selectedItems,
				                              	state: false,
				                            },
				                            url: apiService.apiUrl('updateUsersState'),
				                            needReset: true,
				                            needResetSelected: true,
				                            successCallback: function(data, status, headers, config){
				                              	logger.logSuccess(localize.getLocalizedString("Successfully updated selected users"));
				                            },
				                            failCallback: function(data, status, headers, config){
				                                    
				                            },
				                        });
			                        }
			                    });

		                    }
	                  	}
	                },
	                {
	                  	type: 'divider',
	                },
	                //remove selected
	                {
	                    type: 'tool',
	                    label: localize.getLocalizedString("Remove All Selected"),
	                    clickFunction: {
	                      	type: 'serverRequestForSelected',
	                      function: function(items, selectedItems, requestFunction){
		                        $bootbox.confirm(localize.getLocalizedString("Are you sure you want to remove all selected items?"), function(result) {
		                          	if(result){
			                            requestFunction({
			                              	requestData : {
			                                	accessToken: sessionService.getToken(),
			                                	removeIds: selectedItems,
			                              	},
			                              	url: apiService.apiUrl('removeUsers'),
			                              	needReset: true,
			                              	needResetSelected: true,
			                              	successCallback: function(data, status, headers, config){
			                                	logger.logSuccess(localize.getLocalizedString("Successfully remove selected users"));
			                              	},
			                              	failCallback: function(data, status, headers, config){
			                                      
			                              	},
			                            });
		                          	}
		                        });
	                      	},
	                      	ngShow: function(){
	              				//only show up if have permission
	                  			return permissionService.canRemoveUser();
	                		},
	                    }
	                },
	              ],
	            },
	            //add button
	            {
	                type: 'button',
	                label: localize.getLocalizedString("New Staff"),
	                icon: 'fa fa-plus',
	                clickFunction: {
	                  	type: 'normal',
	                  	function: function(){
	                    	//show modal to add new user
	                    	$modal.open({
	                      		templateUrl: "/views/main/pages/managementModule/hr/user/users/modal/createNewUserModal.html",
	                      		scope: $scope,
	                      		windowClass: 'app-form-modal-window'
	                    	});
	                  	},
	                },
	                ngShow: function(){
	      				//only show up if have permission
	          			return permissionService.canEditUser();
	        		},
	            },
	          ],

	          //configure the search
	          searchConfig: [
	            {
	              	label: localize.getLocalizedString("By Name"),
	              	model: "t.name",
	              	defaultValue: null,
	              	value: null,
	              	type: "text",
	              	searchType: 'like',
	            },
	            {
	              	label: localize.getLocalizedString("By Email"),
	              	model: "email",
	              	defaultValue: null,
	              	value: null,
	              	type: "text",
	              	searchType: 'like',
	            },
	            //by group
	            {
	              	label: localize.getLocalizedString("By Groups"),
	              	model: 'group_list',
	              	defaultValue: null,
	              	value: null,
	              	type: "select2",
	              	searchType: 'multiSelect2ContainAll',
	              	id: "search_by_groups",
	              	itemId: "name",
	              	queryType: 'having',
	              	config:{
	                	allowClear:true,
	                	multiple:true,
	                	placeholder: localize.getLocalizedString("Search staff belong to selected group"),
	                	minimumInputLength: 0,
	                	id : function(item) {
	                    	return item.group_id;
	                	},
	                	ajax : {
	                  		url: apiService.apiUrl('searchPermissionGroup'),
	                  		dataType: 'json',
	                  		params: {
	                      		contentType: undefined
	                  		},
	                  		type: 'POST',
		                  	data: function (term, page) {
		                      	return JSON.stringify({
		                          	accessToken: sessionService.getToken(),
		                          	limit: 10,
		                          	search: term,
		                      	});
		                  	},
		                  	results: function (data, page) { // parse the results into the format expected by Select2.
		                    	// since we are using custom formatting functions we do not need to alter remote JSON data
		                    	return {
		                        	results: data.data.data,
		                    	};
		                  	}
	                	},
		                initSelection: function (element, callback) {
		                  
		                },
		                formatResult: function (item) {
		                  	return "<div>"+item.name+"</div>";
		                },
		                formatSelection: function (item) {
		                  	return item.name;
		                },
	                	dropdownCssClass: "bigdrop",
	                	escapeMarkup: function (m) {
	                  		return m;
	                	},
	              	}
	            },
	            {
	              	label: localize.getLocalizedString("By State"),
	              	model: "t.active",
	              	defaultValue: "",
	              	value: "",
	              	type: "select",
	              	searchType: 'equal',
	              	items:[
		                {
		                  	text: "Active",
		                  	value: "1"
		                },
		                {
		                  	text: "Deactive",
		                  	value: "0"
		                }
	              	],
	            },
	            {
	              	label: localize.getLocalizedString("By Address"),
	              	model: "address",
	              	defaultValue: null,
	              	value: null,
	              	type: "text",
	              	searchType: 'like',
	            },
	            {
	              	label: localize.getLocalizedString("By Phone"),
	              	model: "phone",
	              	defaultValue: null,
	              	value: null,
	              	type: "text",
	              	searchType: 'like',
	            },
	            {
	              	label: localize.getLocalizedString("By Create Date"),
	              	model: "t.create_date",
	              	defaultValue: null,
	              	value: null,
	              	type: "daterange",
	              	searchType: 'daterange',
	            },
	        ],
	    };
	}

	configurations.tblStaffsControls = function() {
	    return {};
	};

	/* ------------------ Create Staff Form configuration ------------------ */

	configurations.createStaffFormConfig = function($scope) {
	    return {
        	name: "addUserForm",
        	token: sessionService.getToken(),

	        style:{
	         	labelWidth: 3,
	        },
        
	        postRequest:{
	          	action: apiService.apiUrl('createUser'),
	          	successCallback: function(data, status, headers, config){
	                $scope.tblStaffsControls.refresh();
	                logger.logSuccess(localize.getLocalizedString("Successfully create new user"));
	            },
	          	failCallback: function(data, status, headers, config){
	                  
	            },
	        },

	        lblSubmit: "Create",

	        fields: [
		        {
		            type: "text",
		            name: "name",
		            label: localize.getLocalizedString("Name"),
		            model: "name",
		            defaultValue: "",
		            placeholder: "User's Name",
		            validation: {
		            	required:false,
		            }
		        },
		        {
		            type: "email",
		            name: "email",
		            label: localize.getLocalizedString("Email"),
		            model: "email",
		            defaultValue: "",
		            placeholder: "User Email",
		            validation: {
		              	required:true,
		              	invalidMessage: "Invalid email format."
		            }
		        },
		        {
		            type: "password",
		            name: "password",
		            label: localize.getLocalizedString("Password"),
		            model: "password",
		            defaultValue: "",
		            placeholder: "User Password",
		            validation: {
		              	required:true,
		              	'data-ng-minlength': 6,
		            }
		        },
		        {
		            type: "password",
		            name: "verifyPassword",
		            label: localize.getLocalizedString("Verify Password"),
		            defaultValue: "",
		            placeholder: "Retype the user password you inputed above",
		            validation: {
		              	required:true,
		              	'data-validate-equals': "password",
		            }
		        },
		        {
		            type: "text",
		            name: "address",
		            label: localize.getLocalizedString("Address"),
		            model: "address",
		            defaultValue: "",
		            placeholder: "User's Address",
		            validation: {
		              	required:false,
		            }
		        },
		        {
		            type: "phone",
		            name: "phone",
		            label: localize.getLocalizedString("Phone"),
		            model: "phone",
		            defaultValue: "",
		            placeholder: "e.g +84973668872",
		            validation: {
		              	required:false,
		            }
		        },
		        {
		            type: "switch",
		            name: "active",
		            label: localize.getLocalizedString("State"),
		            defaultValue: true,
		            model: "active",
		            values:{
		              	on: {
		                	label: localize.getLocalizedString("Active"),
		              	},
		              	off: {
		                	label: localize.getLocalizedString("Deactive"),
		              	}
		            },
		            validation: {
		              	required:false,
		            }
		        },
	        ],
   		};
	};

	configurations.createStaffFormControl = function() {
	    return {};
	};

	/* ------------------ Avatar Uploader configuration ------------------ */
	configurations.avatarUploaderConfig = function() {
	    return {
	        title: localize.getLocalizedString("Upload User Avatar"),
	        model: "file",
	        targetModel: "userId",
	        token: sessionService.getToken(),
	        preview: {
	          size:{
	            width: 200,
	            height: 150
	          }
	        },

	        uploadRequest:{
	            url: apiService.apiUrl('uploadUserAvatar'),
	            successCallback: function(data, status, headers, config){
	                    $scope.tblStaffsControls.refresh();
	            },
	            failCallback: function(data, status, headers, config){
	                    
	            },
	        },
	    };
	};

	configurations.avatarUploaderControls = function() {
	    return {};
	};

	return configurations;
})

/* ========================================== Logic Handler ========================================== */

.controller('usersCtrl', [
    '$scope', 
    'usersConfiguration',
    function($scope, usersConfiguration) {

    	/* ================== PARAMS ================== */
    	$scope.breadcrumbs = usersConfiguration.breadcrumbs();

      	$scope.tblStaffsConfig = usersConfiguration.tblStaffsConfig($scope);
	    $scope.tblStaffsControls = usersConfiguration.tblStaffsControls();

      	$scope.createStaffFormConfig = usersConfiguration.createStaffFormConfig($scope);
      	$scope.createStaffFormControl = usersConfiguration.createStaffFormControl();

      	$scope.avatarUploaderConfig = usersConfiguration.avatarUploaderConfig();
	    $scope.avatarUploaderControls = usersConfiguration.avatarUploaderControls();

        /* ================== PAGE EVENT ================== */

    	/* ================== FUNCTIONS ================== */

        /* ================== PAGE BEHAVIOUS ================== */
    }
]);