angular.module('app.modelServices.userService', []).factory('userService', function(userModel, apiService, locationService, $q, httpRequest, sessionService){
	return	{
		fetchUser: function(id, successHandler, errorHandler){
			var fetchUserData = {
	      		accessToken: sessionService.getToken(),
	      		userId: id,
	      	};
			var fetchDeferred = $q.defer();
    		var requestFetchUser = new httpRequest(apiService.apiUrl("userDetail"), fetchUserData, fetchDeferred.resolve, fetchDeferred.reject);
    		
    		//handle the callback from api call
    		fetchDeferred.promise  
		    .then(function(data, status, headers, config) {
		    	if(data.error == 1){
		    		//redirect user to error page
		    		locationService.errorRedirect(false);

		    		if(errorHandler){
		    			errorHandler(data, status, headers, config);
		    		}
		    	}
		    	else{
		    		//get user data
		    		var user = new userModel(data.data);
		    		if(successHandler){
		    			successHandler(user);
		    		}
		    	}
		    }, function(data, status, headers, config) {
		    	//redirect user to error page
		    	locationService.errorRedirect(false);

		    	if(errorHandler){
	    			errorHandler(data, status, headers, config);
	    		}
		    });

		    //call the request
		    return requestFetchUser.request();
		},
	}
});
