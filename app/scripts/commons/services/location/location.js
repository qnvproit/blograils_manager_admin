//hold the function relate to page changing, or error redirection. In other word, have all url location relate function
angular.module('services.location', []).factory('locationService', function($location){

	var defaultPath = '/dashboard';										//default page to be redirect to
	var defaultErrorPath = '/500';										//default error path when status isn't set
	var defaultKickoutPath = '/signin';									//page when user got kick out due to invalid authentication
	var defaultKickoutPathWithParam = '/signin/kickout/';				//page when user got kick out due to invalid authentication

	return	{
		//default page
		defaultRedirect:	function(){	
	        $location.path(defaultPath);
		},
		//default page when an error happen
		errorRedirect:	function(keepHistory){
			if(keepHistory){
				$location.path(defaultErrorPath);
			}
			else{
				$location.path(defaultErrorPath).replace();
			}
		},
		//redirect base on error
		errorRedirectWithStatus: function(keepHistory, status){
			var path = '/500';

			if(status == 500){
				path = '/500';
			}
			else if(status == 403){
				path = '/403';
			}
			else if(status == 404){
				path = '/404';
			}
			else if(status == 400){
				path = '/400';
			}
			else{
				path = '/500';
			}

			if(keepHistory){
				$location.path(path);
			}
			else{
				$location.path(path).replace();
			}
		},
		kickout: function(message){
			if(message){
				$location.path(defaultKickoutPathWithParam + message);
			}
			else{
				$location.path(defaultKickoutPath);
			}
		},
		//set the default path
		setDefaultPage: function(path){
			defaultPath = path;
		},
		//set the default path
		setDefaultErrorPage: function(path){
			defaultErrorPath = path;
		}
	}
});
