angular.module( 'directives.qPagination', [] ).directive( 'qPagination', [ function() {

	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'scripts/commons/directives/qPagination/template/qPagination.html',
		scope: {
			config: '='
		},
		controller: function( $scope ) {
			// Method get data
			$scope.getDataTable = function() {
				$scope.config.getDataTable();
			}

			// GotoPage
			$scope.goToPage = function( orderPage ) {
				if( orderPage < 0 ) {
					return;
				}
				$scope.config.currentPage = orderPage;
			}

			// GoToFirstPage
			$scope.goToFirstPage = function() {
				$scope.config.currentPage = 1;
			}

			// GotoPrevPage
			$scope.goToPrevPage = function() {
				if( $scope.config.currentPage > 1 ) {
					$scope.config.currentPage--;
				}
			}

			// GoToNextPage
			$scope.goToNextPage = function() {
				if( $scope.config.maxPage != -1 && $scope.config.currentPage >= $scope.config.maxPage ) {
					return;
				}
				$scope.config.currentPage++;
			}

			// GoToLastPage
			$scope.goToLastPage = function() {
				$scope.config.currentPage = $scope.config.maxPage;
			}



			/************************ $watch function ***********************/

			function changeCurrentPage( newValue, oldValue, scope ) {
				if( newValue == oldValue ) {
					return;
				}
				$scope.getDataTable();
			}

			$scope.$watch( 'config.currentPage', changeCurrentPage , true  );

			function changeCurrentEntries( newValue, oldValue, scope ) {
				if( newValue == oldValue ) {
					return;
				}
				if( scope.config.currentPage == 1 ) {
					scope.getDataTable();	
				} else {
					scope.goToFirstPage();
				}
				
			}
			$scope.$watch( 'config.currentEntries', changeCurrentEntries, true );

			//$scope.$watch( 'config.sortDefault', changeCurrentEntries, true );
		}
	}

}] );