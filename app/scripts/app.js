'use strict';

/**
 * @ngdoc overview
 * @name angularCoreApp
 * @description
 * # angularCoreApp
 *
 * Main module of the application.
 */
angular
  .module('app', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angular-loading-bar', 
    'ngAnimate',
    'angularFileUpload',
    'cgBusy',
    'ngBootstrap',
    'ui.bootstrap',
    'mgo-angular-wizard',
    'textAngular',
    'google-maps', 
    'ui.select2', 
    'ui.bootstrap.datetimepicker',
    'xeditable',
    'angularMoment',
    'ui.tree',

    //override
    'override.overrides',

    //localize
    'app.localization',

    //run
    'run.runs',

    //service
    'services.services',

    //directive
    'directives.directives',

    //in-app model
    'app.model',
    'app.modelServices',

    //in-app controls
    'app.main',
    'app.layout',
    'app.authentication',
    'app.management',

    // constant
    'app.constants',
  ])
  .config(function ($routeProvider, $httpProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main/pages/managementModule/dashboard/dashboard.html',
        bodyClass: 'body-home',
        showNav: true,
        bodyStyle: 'pos',
        access:{
          restrict: true,
        }
      })
      .when('/signin', {
        templateUrl: 'views/main/pages/managementModule/authentication/signin.html',
        bodyClass: 'body-special',
        showNav: false,
        bodyStyle: 'pos-authentication',
        access:{
          restrict: false,
        }
      })
      .when('/signin/kickout/:message', {
        templateUrl: 'views/main/pages/managementModule/authentication/signin.html',
        bodyClass: 'body-special',
        showNav: false,
        bodyStyle: 'pos-authentication',
        access:{
          restrict: false,
        }
      })
      .when('/blog/post/list', {
        templateUrl: 'views/main/pages/managementModule/post/listpost.html',
        bodyClass: 'body-home',
        showNav: true,
        bodyStyle: 'pos',
        access:{
          restrict: true,
        }
      })
      .when('/blog/post/create', {
        templateUrl: 'views/main/pages/managementModule/post/createpost.html',
        bodyClass: 'body-home',
        showNav: true,
        bodyStyle: 'pos',
        access:{
          restrict: true,
        }
      })
      .when('/comment/list', {
        templateUrl: 'views/main/pages/managementModule/comment/listcmt.html',
        bodyClass: 'body-home',
        showNav: true,
        bodyStyle: 'pos',
        access:{
          restrict: true,
        }
      })
      .when('/comment/author', {
        templateUrl: 'views/main/pages/managementModule/comment/author.html',
        bodyClass: 'body-home',
        showNav: true,
        bodyStyle: 'pos',
        access:{
          restrict: true,
        }
      })
      .when('/resources/images', {
        templateUrl: 'views/main/pages/managementModule/resources/images/images.html',
        bodyClass: 'body-home',
        showNav: true,
        bodyStyle: 'pos',
        access:{
          restrict: true,
        }
      })
      .when('/resources/videos', {
        templateUrl: 'views/main/pages/managementModule/resources/videos/videos.html',
        bodyClass: 'body-home',
        showNav: true,
        bodyStyle: 'pos',
        access:{
          restrict: true,
        }
      })
      .when('/400', {
        templateUrl: 'views/commons/view_pages/error_400.html',
        bodyClass: 'body-special',
        showNav: false,
        bodyStyle: 'pos-error',
        access:{
          restrict: false,
        }
      })
      .when('/403', {
        templateUrl: 'views/commons/view_pages/error_403.html',
        bodyClass: 'body-special',
        showNav: false,
        bodyStyle: 'pos-error',
        access:{
          restrict: false,
        }
      })
      .when('/404', {
        templateUrl: 'views/commons/view_pages/error_404.html',
        bodyClass: 'body-special',
        showNav: false,
        bodyStyle: 'pos-error',
        access:{
          restrict: false,
        }
      })
      .when('/500', {
        templateUrl: 'views/commons/view_pages/error_500.html',
        bodyClass: 'body-special',
        showNav: false,
        bodyStyle: 'pos-error',
        access:{
          restrict: false,
        }
      })
      .otherwise({
        redirectTo: '/'
      });


    $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
  });
