angular.module('app.mangement.post', [])
.controller('postCtrl', [
    '$scope', 
    'ajaxService', 
    'apiUrl',
    'sessionService',
    '$modal',
    '$bootbox',
    'logger',
    function($scope, ajaxService, apiUrl, sessionService, $modal, $bootbox, logger) {

        $scope.isBusy = false;

        $scope.breadcrumbs = [
            {
                href: '#/posadmin/hr/users',
                label: 'List posts'
            },
        ];
       

        $scope.search = {
            byTitle: '',
            byContent: '',
            byCategory: '',
            byStatus: '',
            byCreated: ''
        }


        // Config qPagination directive
        $scope.pageConfig = {
            getDataTable: function() {
                $scope.getListPost();
                // if ($scope.hasFirstLoad) {
                //     $scope.getListUser();
                // }
            },
            entriesPerPage: [ 2, 5, 10, 15, 20, 25 ],
            currentEntries : 2,
            currentPage: 1,
            maxPage: -1,
            items: 0,
        }


        // Config qSortCols directive
        $scope.sortConfig = {
            sortBy: 'title',
            asc: true,
            changeSort: function( attr, asc ) {
                $scope.sortConfig.sortBy = attr;
                $scope.sortConfig.asc = asc;
                $scope.getListPost();
            }
        }


        $scope.getListPost = function() {
            $scope.isBusy = true;

            ajaxService.request(
                apiUrl.listPost,
                {
                    access_token: sessionService.getToken(),
                    page: $scope.pageConfig.currentPage,
                    limit: $scope.pageConfig.currentEntries,
                    sortBy:$scope.sortConfig.sortBy,
                    asc: $scope.sortConfig.asc,
                    search: $scope.search

                }, function(data) {
                    console.log(data);
                    $scope.isBusy = false;
                    if(data.success == true) {
                        $scope.response = data.data;
                        //$scope.listPosts = data.data.data;
                    }
                }, function(data) {

                }
            )
        }

        $scope.deletePost = function(id) {

            $bootbox.confirm("<h4>Are you sure you want to delete this Post ?<h4>", function(result) {
                if( result ) {
                    ajaxService.request(
                        apiUrl.delPost,
                        {
                            access_token: sessionService.getToken(),
                            id: id
                        }, function(data) {
                            if(data.success) {
                                logger.log(data.message);
                            } else {
                                logger.logError(data.message);
                            }
                            $scope.getListPost();
                        }, function(data) { 

                        }
                    );
                    

                }
            });





            
        }


        $scope.showModalAdvandedSearch = function() {
            $scope.advanceSearchModal = $modal.open({
                templateUrl: "views/main/pages/managementModule/post/partials/modal/search.html",
                scope: $scope
            })
        }


        $scope.triggerSearch = function() {
            $scope.getListPost();
            $scope.advanceSearchModal.dismiss('cancel');
        }

        $scope.resetSearch = function() {
            $scope.search.byTitle = "";
            $scope.search.byContent = "";
            $scope.search.byCreated = "";

            $scope.triggerSearch();
        }

        $scope.$watch(function() {
            return $scope.search.byCategory + $scope.search.byStatus;
        }, $scope.getListPost, true);

        $scope.getListPost();

    }
]);