angular.module('directives.breadcrumbs', [])

.directive('breadcrumbs', [
    function() {
      return {
        restrict: 'E',
        scope: {
            items: '=',
        },
        template: '<div class="breadcrumbs"><ul><li ng-repeat=\'bc in breadcrumbs\'><i class="fa fa-home" ng-if="$first"></i><a ng-click="unregisterBreadCrumb( $index )" ng-href="{{bc.href}}"><span data-i18n="{{bc.label}}"></span></a><i class="fa fa-angle-right" ng-if="!$last"></i></li></ul></div>',
        replace: true,
        compile: function(tElement, tAttrs) {
            return function($scope, $elem, $attr) {
	            tAttrs.$observe('items', function(data) {
	                    resetCrumbs();
	            }, true);
              
                var bc_id = $attr['id'],
                    resetCrumbs = function() {
                        $scope.breadcrumbs = [];
                        angular.forEach($scope.items, function(v) {
                            $scope.breadcrumbs.push(v);
                        });
                    };
                resetCrumbs();
            }
        }
      };
    }
]);