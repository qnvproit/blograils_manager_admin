angular.module('directives.singleImageUploader', []).directive('singleImageUploader', [
  function() {
    return {
      restrict: 'A',
      scope: {
          config: '=',
          control: '=',
          target: '=',
          image: '@image',
          target: '@target',
      },
      controller: function($scope, $location, $modal, $element, $http){
        $scope.templatePath = 'scripts/commons/directives/imageUploader/singleImageUploader/v1.0/'

        /* ==================== CONFIGURE PARAMS ===================== */
        $scope.title = $scope.config.title; 
        $scope.model = $scope.config.model;
        $scope.fileKey = ($scope.config.fileKey)?$scope.config.fileKey:'file';           
        $scope.preview = $scope.config.preview;    
        $scope.targetModel = $scope.config.targetModel;
        $scope.token = $scope.config.token;
        $scope.uploadRequest = $scope.config.uploadRequest;

        /* ==================== PARAMS ===================== */
        $scope.uploadStatus = {
          status: null,                       //null, fail, success, uploading
          message: null,
        };

        $scope.uploadData = {
          accessToken: $scope.token,
        };
        $scope.uploadData[$scope.targetModel] = $scope.target;
        $scope.uploadData[$scope.fileKey] = null;

        /* ==================== FUNCTIONS ===================== */
        //handler when request success but fail
        $scope.requestSuccessWithErrorHandler = function(data, status, headers, config){
          if(data.error == 1){
            $scope.uploadStatus.status = 'fail';
            $scope.uploadStatus.message = data.message;
          }
        };

        $scope.requestFailHandler = function(data, status){
          $scope.uploadStatus.status = 'fail';
          $scope.uploadStatus.message = data.message;
        };

        $scope.requestSuccessHandler = function(data, status, headers, config){
          $scope.uploadStatus.status = 'success';
          $scope.uploadStatus.message = data.message;
        };

        $scope.reset = function(){
          $scope.uploadData[$scope.fileKey] = null;

          $scope.uploadStatus = {
            status: null,                       
            message: null,
          }
        };

        //force open the modal
        $scope.open = function(targetId, defaultImage){
          if(defaultImage){
            $scope.image = defaultImage;
          }
          $scope.target = targetId;
          $scope.uploadData[$scope.targetModel] = $scope.target;

          $scope.reset();

          $scope.advanceSearchModal = $modal.open({
            templateUrl: $scope.templatePath + 'tpl/singleImageUploader.html',
            scope: $scope,
          });
        };

        $scope.upload = function(){
          $scope.uploadStatus.status = 'uploading';

          var formData = new FormData();
          angular.forEach($scope.uploadData, function(value, key) {
            formData.append(key, value);
          });

          $scope.tblLoader = $http.post($scope.uploadRequest.url, formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
          })
          .success(function(data, status, headers, config) {
            if(data.error == 1){
              $scope.requestSuccessWithErrorHandler(data, status, headers, config);

              if($scope.uploadRequest.failCallback != null){
                $scope.uploadRequest.failCallback(data, status, headers, config);
              }
            }
            else{
              $scope.requestSuccessHandler(data, status, headers, config);

              //get the user data
              if($scope.uploadRequest.successCallback != null){
                var itemsData = $scope.uploadRequest.successCallback(data, status, headers, config);
              }
            }
          })
          .error(function(data, status, headers, config) {
            $scope.requestFailHandler(data, status);

            if($scope.uploadRequest.failCallback != null){
              $scope.uploadRequest.failCallback(data, status, null, null);
            }
          });
        };

        /* ==================== DEFINE CONTROLS ===================== */
        if ($scope.control) {
          $scope.control.open = $scope.open;
        };

        /* ==================== EVENT HANDLER ===================== */
        $element.bind('click', function() {
          $scope.reset();

          $scope.advanceSearchModal = $modal.open({
            templateUrl: $scope.templatePath + 'tpl/singleImageUploader.html',
            scope: $scope,
          });
        });
      },
      compile: function(tElement, tAttrs) {
          return function($scope, $elem, $attr) {
              
          }
      }
    };
  }
]);
