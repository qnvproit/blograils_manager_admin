angular.module('directives.directives', [
	'directives.page',
	'directives.bootbox',
	'directives.breadcrumbs',
	'directives.dataTable',
	'directives.bosForm',
	'directives.bosFormFields',
	'directives.singleImageUploader',
	'directives.bosMap',
    'directives.qBusy',
    'directives.qPagination',
    'directives.qSortCols',
    'directives.qTable',
    'directives.qTags',
    'directives.qEnter',
    'directives.qAutoComplete',
    'directives.ngThumb'
]);