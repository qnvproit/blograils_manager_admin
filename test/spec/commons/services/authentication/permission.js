describe('Service:permissions tests', function () {
    var permissionService;
    var $window;
    var $location;
    var mockSessionService;
    var userPermissions;

    beforeEach(function(){  
        module('services.permissions');

        //we need to fake the session service since this is the custom service.
        mockSessionService = {
            getUserData: function () {
                return {user_id: 1};
            },
            getPermissions: function (){
                return userPermissions;
            }
        };

        module(function ($provide) {
            $provide.value('sessionService', mockSessionService);
        });

        inject(function (_permissionService_, _$window_, _$location_) { 
            permissionService = _permissionService_;
            $window = _$window_;
            $location = _$location_;
        })

        userPermissions = [
            'PERMISSION_1',
            'PERMISSION_2',
            'PERMISSION_3',
        ];

        //now that our $window object is populated, set up the mock and spies for its localStorage property:
        window.localStorageMockSpy.setup($window); 
    });

    it('Should redirect user to 403 error page when permission error called', function () {
        spyOn($location, 'path');    
        permissionService.permissionError();
        expect($location.path).toHaveBeenCalledWith('/403');
    });

    it('Should return true when user have all permissions that require, or false when missing one permissions', function () {
        $window.localStorage.permissions = angular.fromJson(userPermissions);

        var requirePermission = [
            'PERMISSION_1',
            'PERMISSION_2',
        ];

        //in this case, user have enough permission as require , should return true
        expect(permissionService.havePermissions(requirePermission)).toEqual(true);

        var requirePermission = [
            'PERMISSION_1',
            'PERMISSION_2',
            'PERMISSION_4',
        ];

        //in this case, user don't enough permission as require, missing one , should return false
        expect(permissionService.havePermissions(requirePermission)).toEqual(false);
    });

    it('Should return true when user have atleast one permission that required, or false when missing all permissions', function () {
        $window.localStorage.permissions = angular.fromJson(userPermissions);

        var requirePermission = [
            'PERMISSION_1',
            'PERMISSION_4',
        ];

        //in this case, user have enough permission as require , should return true
        expect(permissionService.haveOneOfPermissions(requirePermission)).toEqual(true);

        var requirePermission = [
            'PERMISSION_11',
            'PERMISSION_21',
            'PERMISSION_41',
        ];

        //in this case, user don't enough permission as require, missing one , should return false
        expect(permissionService.haveOneOfPermissions(requirePermission)).toEqual(false);
    });

    it('Should return right value when user check if the request id is himself or not', function () {
        var requestId = 1;

        //in this case, the id match what return from user session id, so expect true
        expect(permissionService.isMyself(requestId)).toEqual(true);

        var requestId = 2;

        //in this case, the id don't match what return from user session id, so expect true
        expect(permissionService.isMyself(requestId)).toEqual(false);
    });
});