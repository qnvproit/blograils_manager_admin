describe('Service:HttpRequest tests', function () {
    var httpRequest;
    var $http;
    var $q;
    var $timeout;
    var $httpBackend;

    var domain = 'http://posapi.bos-vn.localsite/';

    beforeEach(function(){  
        module('services.http');

        inject(function (_httpRequest_, _$http_, _$q_, _$timeout_, _$httpBackend_) { 
            httpRequest = _httpRequest_;
            $http = _$http_;
            $q = _$q_;
            $timeout = _$timeout_;
            $httpBackend = _$httpBackend_;
        })
    });

    it('The url, data should be set when called', function () {
        var url = 'requestUrl';
        var data = 'requestData';

        var request = new httpRequest(url, data, null, null);
        expect(request.url).toEqual(url);
        expect(request.data).toEqual(data);
    });

    it('Without url, make sure return fail', function () {
        var request = new httpRequest(null, null, null, null);
        expect(request.request()).toEqual(false);
    });

    it('When request is called and return successfully, make sure the success handler is called, and all success data is set as expect', function () {
        $httpBackend.when('POST', domain + 'auth/loginApi')
        .respond(200, {
            error: 0,
            message: "Login Successfully.",
            data: {
                access_token: "XOGzgkrFdrAJdzGUz7x8UtIyEZkr0d",
                active: "1",
                address: "2802 Chung cu song da, Nguyen Trai, Thanh Xuan, Ha Noi",
                avatar: "zYh8WpuCkbFEho6SEVem.jpg",
                avatar_url: "http://posapi.bos-vn.localsite/userdata/images/roxwin/avatar//zYh8WpuCkbFEho6SEVem.jpg",
                create_date: "1397581200",
                email: "quocviet.cntt.bk@gmail.com",
                name: "Viet Quoc",
                permissions: '["BROWSER_POS_USER","EDIT_POS_USER","REMOVE_POS_USER","BROWSER_POS_GLO_PER","BROWSER_POS_CUSTOMER","EDIT_POS_CUSTOMER","REMOVE_POS_CUSTOMER","BROWSER_POS_ORDER","EDIT_POS_ORDER","REMOVE_POS_ORDER","BROWSER_POS_PRODUCT","EDIT_POS_PRODUCT","REMOVE_POS_PRODUCT","BROWSER_POS_ALL_SHOP","EDIT_POS_SHOP","REMOVE_POS_SHOP","BROWSER_POS_SHOP","EDIT_EVENT_SHOP","REMOVE_EVENT_SHOP","SYSTEM_FINANCIAL_ACCESS","VIEW_FINANCIAL_DATA","EDIT_FINANCIAL_DATA","BROWSER_EVENT_SHOP","BROWSER_EVENT_SYSTEM"]',
                phone: "+84973668874",
                tenant_logo: "http://bos.localsite/images/system/Client/Logo/Agritrack.png",
                user_id: "1",
            },
        });

        var isSuccessHandlerCalled = false;
        var successHandler = function(){
            isSuccessHandlerCalled = true;
        }
        var request = new httpRequest(domain + 'auth/loginApi', 'sample data', successHandler, null);
        request.request();

        $httpBackend.flush();

        //the did request will be set to true
        expect(request.didRequest).toEqual(true);

        //the last request result have to be true (successfully)
        expect(request.lastRequestResult).toEqual(true);

        //status have to be 200
        expect(request.lastRequestStatus).toEqual(200);

        //success handler have to be called
        expect(isSuccessHandlerCalled).toEqual(true);
    });

    it('When request is called but result in failure, make sure all the failure status is handled as expect', function () {
        $httpBackend.when('POST', domain + 'auth/loginApi')
        .respond(500, {
            error: 1,
            message: "Authentication Fail! Please recheck your authentication data!",
        });

        var isFailureHandlerCalled = false;
        var failureHandler = function(){
            isFailureHandlerCalled = true;
        }
        var request = new httpRequest(domain + 'auth/loginApi', 'sample data', null, failureHandler);
        request.request();

        $httpBackend.flush();

        //the did request will be set to true
        expect(request.didRequest).toEqual(true);

        //the last request result have to be false (failure)
        expect(request.lastRequestResult).toEqual(false);

        //status have to be 200
        expect(request.lastRequestStatus).toEqual(500);

        //success handler have to be called
        expect(isFailureHandlerCalled).toEqual(true);
    });
});