//the object which we will use to call any http request.
angular.module('services.logger', []).factory('logger', function(){
    var logIt;
    toastr.options = {
        "closeButton": true,
        "positionClass": "toast-top-right",
        "timeOut": "3000"
    };
    logIt = function(message, type) {
        return toastr[type](message);
    };
    return {
        log: function(message) {
          logIt(message, 'info');
        },
        logWarning: function(message) {
          logIt(message, 'warning');
        },
        logSuccess: function(message) {
          logIt(message, 'success');
        },
        logError: function(message) {
          logIt(message, 'error');
        }
    };
});
