//hold every page relate data. Like will show navigation, will show breadcrum,....
angular.module('services.permissions', []).factory('permissionService', function($location, $window, sessionService){
return	{
	permissionError: function(){
		$location.path('/403');
	},
	havePermissions: function(permissions){
		if(permissions && permissions.length > 0){
			var hasPermission = true;
			var userPermissions = sessionService.getPermissions();
			var continueLoop = true;
			angular.forEach(permissions, function(value, key) {
	            if(value && continueLoop){
	              if(($.inArray(value, userPermissions) <= -1)){
	              	hasPermission = false;
	              	continueLoop = false;
	              }
	            }
	        });
            return hasPermission;
		}
		else{
			return true;
		}
	},
	haveOneOfPermissions: function(permissions){
		if(permissions && permissions.length > 0){
			var hasPermission = false;
			var userPermissions = sessionService.getPermissions();
			var continueLoop = true;
			angular.forEach(permissions, function(value, key) {
	            if(value && continueLoop){
	              if(($.inArray(value, userPermissions) >= 0)){
	              	hasPermission = true;
	              	continueLoop = false;
	              }
	            }
	        });
            return hasPermission;
		}
		else{
			return false;
		}
	},
	//is this user the current user
	isMyself: function(id){
		if(id == sessionService.getUserData().user_id){
      		return true;
      	}
      	else{
      		return false;
      	}
	},
	canBrowserUser:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('BROWSER_POS_USER', permissions) > -1)
	},
	canEditUser:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('EDIT_POS_USER', permissions) > -1)
	},
	canRemoveUser:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('REMOVE_POS_USER', permissions) > -1)
	},
	canBrowserSystemPermission:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('BROWSER_POS_GLO_PER', permissions) > -1)
	},
	canBrowserProduct:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('BROWSER_POS_PRODUCT', permissions) > -1)
	},
	canBrowserCustomer:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('BROWSER_POS_CUSTOMER', permissions) > -1)
	},
	canEditCustomer:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('EDIT_POS_CUSTOMER', permissions) > -1)
	},
	canRemoveCustomer:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('REMOVE_POS_CUSTOMER', permissions) > -1)
	},
	canBrowserOrder:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('BROWSER_POS_ORDER', permissions) > -1)
	},
	canEditOrder:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('EDIT_POS_ORDER', permissions) > -1)
	},
	canRemoveOrder:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('REMOVE_POS_ORDER', permissions) > -1)
	},
	canViewAllShop:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('BROWSER_POS_ALL_SHOP', permissions) > -1)
	},
	canBrowserShop:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('BROWSER_POS_SHOP', permissions) > -1)
	},
	canEditShop:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('EDIT_POS_SHOP', permissions) > -1)
	},
	canRemoveShop:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('REMOVE_POS_SHOP', permissions) > -1)
	},
	canViewSystemEvent:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('BROWSER_EVENT_SYSTEM', permissions) > -1)
	},
	canViewShopEvent:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('BROWSER_EVENT_SHOP', permissions) > -1)
	},
	canEditEvent:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('EDIT_EVENT_SHOP', permissions) > -1)
	},
	canRemoveEvent:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('REMOVE_EVENT_SHOP', permissions) > -1)
	},
	canViewShopEvent:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('BROWSER_EVENT_SHOP', permissions) > -1)
	},
	canRemoveShop:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('REMOVE_POS_SHOP', permissions) > -1)
	},
	canRemoveShop:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('REMOVE_POS_SHOP', permissions) > -1)
	},
	canAccessSystemFinancial:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('SYSTEM_FINANCIAL_ACCESS', permissions) > -1)
	},
	canViewShopFinancial:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('VIEW_FINANCIAL_DATA', permissions) > -1)
	},
	canEditShopFinancial:	function(){	
        var permissions = sessionService.getPermissions();
        return ($.inArray('EDIT_FINANCIAL_DATA', permissions) > -1)
	},
}
});
