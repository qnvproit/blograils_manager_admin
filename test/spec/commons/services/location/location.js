describe('Service:location tests', function () {
    var locationService;
    var $location;

    beforeEach(function(){  
        module('services.location');

        inject(function (_locationService_, _$location_) { 
            locationService = _locationService_;
            $location = _$location_;
        })
    });

    it('Default redirect have to match the given path', function () {
        //this is default page when user need to be redirect
        var defaultPath = 'path';

        //set the default path
  		locationService.setDefaultPage(defaultPath);

        //now make sure the user get redirect to right page
        spyOn($location, 'path');    
        locationService.defaultRedirect();
        expect($location.path).toHaveBeenCalledWith(defaultPath);
    });

    it('Error redirect without status have to go to default error page', function () {
        //this is default page when user need to be redirect
        var defaultPath = 'error_path';

        //set the default path
        locationService.setDefaultErrorPage(defaultPath);

        //now make sure the user get redirect to right page
        spyOn($location, 'path');    
        locationService.errorRedirect(true);
        expect($location.path).toHaveBeenCalledWith(defaultPath);
    });

    it('Error redirect need to go to the right page', function () {
        var statuses = [400, 403, 404, 500];
        var paths = ['/400', '/403', '/404', '/500'];

        spyOn($location, 'path');   

        for(var i = 0; i < statuses.length; i++){
            var status = statuses[i];
            var path = paths[i];
 
            locationService.errorRedirectWithStatus(true, status);
            expect($location.path).toHaveBeenCalledWith(path);
        }

        //this one will be an invalid status, we will need to make sure it go to 500
        var wrongStatus = 122;
        var path500 = '/500';
  
        locationService.errorRedirectWithStatus(true, wrongStatus);
        expect($location.path).toHaveBeenCalledWith(path500);

    });
});