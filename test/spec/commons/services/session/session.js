describe('Service:session tests', function () {
    var sessionService;
    var $window;
    var fakePermissions;
    var $location;

    beforeEach(function(){  
        module('services.session');

        inject(function (_sessionService_, _$window_, _$location_) { 
            sessionService = _sessionService_;
            $window = _$window_;
            $location = _$location_;
        })

        //set up our fake permissions
        fakePermissions = [
            'PERMISSION_1',
            'PERMISSION_2',
            'PERMISSION_3',
        ];

        //now that our $window object is populated, set up the mock and spies for its localStorage property:
        window.localStorageMockSpy.setup($window); 
    });

    it('Should save user permission into local storage successfully and then get the same value after that', function () {
  		//make a copy
        var userPermissions = fakePermissions;

        //save into localstorage then get back to check
        sessionService.savePermissions(angular.toJson(userPermissions));
        expect(sessionService.getPermissions()).toEqual(userPermissions);
    });

    it('Should return right value when check user authentication', function () {
  		//set the user token
  		sessionService.saveToken('random_token');

        //need the anthentication check to return true
        expect(sessionService.isAuthenticated()).toEqual(true);

        //remove the token
  		sessionService.removeToken();

        //now the check for authentication need to return false
        expect(sessionService.isAuthenticated()).toEqual(false);
    });

    it('Should be redirect when authentication fail', function () {
        //remove the token
  		sessionService.removeToken();

        //now make sure the user get redirect to signin page
        spyOn($location, 'path');    
        sessionService.checkAuthenticated();
        expect($location.path).toHaveBeenCalledWith('/signin');
    });
});