angular.module('services.services', [
	'services.pageSetting', 
	'services.session', 
	'services.permissions', 
	'services.authentication', 
	'services.location',
	'services.api',
    'services.ajax',
	'services.http',
	'services.logger',

	'services.configs',
]);