(function() {
  'use strict';
  angular.module('app.main', []).controller('MainCtrl', [
    '$scope', 
    'pageSettingService', 
    'sessionService', 
    'locationService', 
    'apiService', 
    'authenticationService', 
    function($scope, pageSettingService, sessionService, locationService, apiService, authenticationService) {
      /* =============== global config ================ */
      //set the default page
      locationService.setDefaultPage('/');

      //set the api domain
      apiService.setDomain('http://localhost:9000');

      /* =============== global params ================ */
      $scope.global = {
        brand: 'Qlog',
        fullbrand: 'Qlog Manager',
        name: (sessionService.getName())?sessionService.getName():'Guest',
        logo: sessionService.getLogo(),
        avatar: (sessionService.getUserData())?sessionService.getUserData().avatar_url:'/images/assets/default/userAvatar.png',
      };

      /* =============== global functions ================ */
      /* check if the template is the selected type or not */
      $scope.isTemplate = function(name){
        return angular.equals(pageSettingService.template(), name);
      };

      /* get the class of the body */
      $scope.getBodyClass = function(){
        return pageSettingService.bodyClass();
      };

      /* check if we will show navication or not */
      $scope.showNav = function(){
        return pageSettingService.willShowNav();
      };

      /* Redirect to the default page of the site. To change default page, go to location service */
      $scope.redirectToHomePage = function(){
        locationService.defaultRedirect();
      };

      /* Logout of the system, user will be redirected to signin page */
      $scope.logout = function(){
        authenticationService.signOut(function(){}, function(){});
      };
    }
  ]);

}).call(this);
