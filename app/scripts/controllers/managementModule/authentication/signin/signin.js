angular.module('app.authentication.signin', [])

.controller('signinCtrl', [
    '$scope', 'authenticationService', '$q', 'logger', 'locationService', '$routeParams', 'sessionService', '$http', function($scope, authenticationService, $q, logger, locationService, $routeParams, sessionService, $http) {
     
        /* ================== GET ROUTER PARAMS =====================*/
        var kickOutMessage = $routeParams.message;

    	/* ================== PARAMS ================== */
    	$scope.requestSignin = null;										//request object to call signin api
    	$scope.showFailureMessage = false;									//flag to check if the message failure should show
    	$scope.failureMessage = '';											//failure message when failed
        $scope.busyLoad = null;												//loader when called

    	//store the user login data
        $scope.loginData = {
            email: '',
            password: '',
        };

        $scope.login = function() {
            $http({
                method: "POST",
                url: "http://localhost:9000/api/admins/login",
                data: $scope.loginData
            }).then(function(data) {
                console.log(data.data);

                if(data.data.success == false){
                    //remove token incase of failure
                    sessionService.removeToken();

                    $scope.showFailureMessage = true;
                    $scope.failureMessage = data.data.message;
                }
                else{
                    //store user data
                    var userData = angular.fromJson(data.data.data);
                    sessionService.saveToken(userData.access_token);
                    sessionService.saveName(userData.username);
                    // console.log(userData.username);
                    sessionService.saveUserData(angular.toJson(data.data.data));

                    // successHandler();
                    $scope.hideFailureMessage();

                    //show successfully message
                    logger.logSuccess("We are loading your personal data. Please wait a bit...");

                    //redirect to homepage
                    locationService.defaultRedirect();
                }

            }, function() {
                console.log(data);

                //remove token incase of failure
                sessionService.removeToken();
                    
                //show the message
                $scope.showFailureMessage = true;
                $scope.failureMessage = message;
            })
        }



        /* ================== PAGE EVENT ================== */
        /**
        *   Called when page finish loading
        */
        $scope.onPageDidLoad = function(){
            if(kickOutMessage && kickOutMessage.length > 0){
              logger.logError(kickOutMessage);
            }
        }

    	/* ================== FUNCTIONS ================== */

    	/**
    	* Hide the failure message
    	*/
    	$scope.hideFailureMessage = function() {
          $scope.showFailureMessage = false;
        };

        /**
    	* Check if the form can be submitted
    	*/
        $scope.canSubmit = function() {
          return $scope.form_sign_in.$valid && !$scope.form_sign_in.$pristine;
        };

    	/**
    	* Call the signin api
    	*/
    	$scope.signin = function(){
    		var signinDeferred = $q.defer();

    		//handle the callback from api call
    		signinDeferred.promise  
		    .then(function() {
		    	$scope.hideFailureMessage();

                //show successfully message
                logger.logSuccess("We are loading your personal data. Please wait a bit...");

                //redirect to homepage
                locationService.defaultRedirect();
		    }, function(message) {
		    	//show the message
		    	$scope.showFailureMessage = true;
		    	$scope.failureMessage = message;
		    });

		    //call the request
		    $scope.busyLoad = authenticationService.signIn($scope.loginData, signinDeferred.resolve, signinDeferred.reject);
    	}

        /* ================== PAGE BEHAVIOUS ================== */
        /**
        *   Page finish load
        */
        $scope.onPageDidLoad();
    }
]);