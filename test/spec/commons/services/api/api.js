describe('Service:Api tests', function () {
    var apiService;

    beforeEach(function(){  
        module('services.api');

        inject(function (_apiService_) { 
            apiService = _apiService_;
        })
    });

    it('Should return right value of domain after we change it to another', function () {
        var domain = 'testDomain';

        apiService.setDomain(domain);
        expect(apiService.domain()).toEqual(domain);
    });
});