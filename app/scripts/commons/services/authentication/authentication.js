//hold every page relate data. Like will show navigation, will show breadcrum,....
angular.module('services.authentication', []).factory('authenticationService', function($location, httpRequest, apiService, $q, sessionService, locationService){
	return	{
		signIn: function(loginData, successHandler, failureHandler){
			var signinDeferred = $q.defer();
    		var requestSignin = new httpRequest(apiService.apiUrl("signin"), loginData, signinDeferred.resolve, signinDeferred.reject);
    		
    		//handle the callback from api call
    		signinDeferred.promise  
		    .then(function(data, status, headers, config) {
		    	if(data.error == 1){
		    		//remove token incase of failure
		    		sessionService.removeToken();

		    		failureHandler(data.message);
		    	}
		    	else{
		    		//store user data
	                var userData = angular.fromJson(data.data);
	                sessionService.saveToken(userData.access_token);
	                sessionService.saveLogo(userData.tenant_logo);
	                sessionService.saveName(userData.name);
	                sessionService.saveTenant(loginData.tenant);
	                sessionService.savePermissions(userData.permissions);
	                sessionService.saveUserData(angular.toJson(data.data));

		    		successHandler();
		    	}
		    }, function(data, status, headers, config) {
		    	//remove token incase of failure
	    		sessionService.removeToken();
		    		
		    	failureHandler(data.message);
		    });

		    //call the request
		    return requestSignin.request();
		},

		/* signout the user, he will then be redirected to signin page */
		signOut: function(successHandler, failureHandler){
			var signoutDeferred = $q.defer();
			var signoutData = {
	          	accessToken : sessionService.getToken(),
	        };
    		var requestSignout = new httpRequest(apiService.apiUrl("signout"), signoutData, signoutDeferred.resolve, signoutDeferred.reject);
    		
    		//handle the callback from api call
    		signoutDeferred.promise  
		    .then(function(data, status, headers, config) {
		    	if(data.error == 1){
		    		sessionService.removeToken();
              		locationService.kickout(null);
              		if(failureHandler){
              			failureHandler(data.message);
              		}
		    	}
		    	else{
		    		sessionService.removeToken();
              		locationService.kickout(null);
              		if(successHandler){
              			successHandler(data.message);
              		}
		    	}
		    }, function(data, status, headers, config) {
		    	sessionService.removeToken();
          		locationService.kickout(null);
          		if(failureHandler){
          			failureHandler(data.message);
          		}
		    });

		    //call the request
		    return requestSignout.request();
		}
	}
});
