/*
*   Author: Qnvproit
*/

angular.module('services.ajax', []).factory('ajaxService', function($http, $q, $timeout, $location){
    
    return {
        request: function(url, data, successCallback, errorCallback ) {
            return $http.post(
                url,
                data
            ).success(function(data) {
                console.log(data);
                successCallback(data);
            }).error(function(data, status, header, config) {
                console.log(data);
                if(status == 401) {
                    $location.path('/signin/kickout/'+ "Invalid access token. Did you access the account from another device?");
                    return;
                }
                errorCallback(data, status, header, config);
            })
        }
    }


});
