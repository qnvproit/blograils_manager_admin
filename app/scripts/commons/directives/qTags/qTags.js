angular.module( 'directives.qTags', [] ).directive( 'qTags', [ function() {

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'scripts/commons/directives/qTags/template/qTags.html',
        scope: {
            options: '=',
            tags: '='
        },
        controller: function( $scope ) {
            console.log($scope.options);

            $scope.inputTag = "";
            $scope.filterOptions = [];

            $scope.addTag = function(tag) {
                tag = tag.trim();
                if($scope.tags.indexOf(tag) == -1) {
                    $scope.tags.push(tag);   
                    $scope.removeFromOptions(tag) ;
                    console.log($scope.options);
                    $scope.inputTag = "";
                }
            }

            $scope.removeFromOptions = function(tag) {
                tag = tag.trim();
                index = $scope.options.indexOf(tag);
                if(index != -1) {
                    $scope.options.splice(index, 1);;
                }
            }

            $scope.removeDoneTags = function(tag) {
                tag = tag.trim();
                index = $scope.tags.indexOf(tag);
                if(index != -1) {
                    $scope.tags.splice(index, 1);
                    $scope.options.push(tag);
                }
            }

            $scope.enterNewTag = function() {
                index = $scope.tags.indexOf($scope.inputTag.trim());
                if(index == -1) {
                    $scope.tags.push($scope.inputTag.trim());
                    
                }
                $scope.inputTag = "";
                
            }

        }
    }

}] );